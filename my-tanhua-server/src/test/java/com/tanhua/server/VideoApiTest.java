package com.tanhua.server;

import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.dubbo.server.api.QuanZiApi;
import com.tanhua.dubbo.server.api.VideoApi;
import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.Video;
import com.tanhua.server.Service.VideoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class VideoApiTest {

    @Reference(version = "1.0.0")
    private VideoApi videoApi;

   @Autowired
    private VideoService videoService;


    @Test
    public void saveVideo() {
        PageInfo<Video> videoPageInfo = videoApi.queryVideoList(1L, 1, 10);
        List<Video> recommendUser = videoPageInfo.getRecommendUser();
        recommendUser.forEach(System.out::println);
    }

    @Test
    public void queryVideoList() {
    }
}