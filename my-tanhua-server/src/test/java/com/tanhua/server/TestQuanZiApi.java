package com.tanhua.server;

import cn.hutool.core.collection.ListUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.dubbo.server.api.QuanZiApi;
import com.tanhua.dubbo.server.pojo.Comment;
import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.Publish;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


public class TestQuanZiApi {

    @Reference(version = "1.0.0")
    private QuanZiApi quanZiApi;

    @Test
    public void testSavePublish(){
        Publish publish = new Publish();
        publish.setText("1anma0:用少数人的思想,做少数人的事,成为极少数人。");
        publish.setMedias(ListUtil.toList("https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/photo/6/1.jpg", "https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/photo/6/CL-3.jpg"));
        publish.setUserId(1L);
        publish.setSeeType(1);
        publish.setLongitude("116.350426");
        publish.setLatitude("40.066355");
        publish.setLocationName("湖南长沙岳麓区东方红中路");
        this.quanZiApi.savePublish(publish);
    }
    @Test
    public void testLike(){
        Long userId = 71L;
        String publishId = "5fae53927e52992e78a3aed9";

//        Publish publish = this.quanZiApi.queryPublishById(publishId);
//        System.out.println(publish);
//
        Long likeCount1 = this.quanZiApi.getLikeCount(publishId);
        System.out.println(likeCount1);
        boolean b = quanZiApi.likeComment(userId, publishId);
        System.out.println(b);
        Boolean data = this.quanZiApi.UserIsLike(userId, publishId);
        System.out.println(data);

        Long likeCount2 = this.quanZiApi.getLikeCount(publishId);
        System.out.println(likeCount2);
        boolean b1 = this.quanZiApi.removeLikeComment(userId, publishId);
        System.out.println(b1);
        Long likeCount3 = this.quanZiApi.getLikeCount(publishId);
        System.out.println(likeCount3);
    }

    @Test
    public void testLove(){
        Long userId = 71L;
        String publishId = "5fae53927e52992e78a3aed9";

//        Publish publish = this.quanZiApi.queryPublishById(publishId);
//        System.out.println(publish);
//
        Long likeCount1 = this.quanZiApi.getLoveCount(publishId);
        System.out.println(likeCount1);
        boolean b = quanZiApi.loveComment(userId, publishId);
        System.out.println(b);
        Boolean data = this.quanZiApi.UserIsLove(userId, publishId);
        System.out.println(data);

        Long likeCount2 = this.quanZiApi.getLoveCount(publishId);
        System.out.println(likeCount2);
        boolean b1 = this.quanZiApi.removeLoveComment(userId, publishId);
        System.out.println(b1);
        Long likeCount3 = this.quanZiApi.getLoveCount(publishId);
        System.out.println(likeCount3);
    }


    @Test
    public void testComent(){
//        Publish publish = this.quanZiApi.queryPublishById(publishId);
//        System.out.println(publish);

//        Boolean aBoolean = quanZiApi.saveComment("5fae53927e52992e78a3aed9", 71L, "真棒呀，小伙子加油");
//        System.out.println(aBoolean);

//        PageInfo<Comment> commentPageInfo = quanZiApi.queryCommentList("5fae53927e52992e78a3aed9", 1, 10);
//        List<Comment> recommendUser = commentPageInfo.getRecommendUser();
//        recommendUser.forEach(System.out::println);


//
        String publishId="5faf45fa7e52993ad06f8455";
        Long userId = 1L;
        Long likeCount1 = this.quanZiApi.getLikeCount(publishId);
        System.out.println(likeCount1);
        boolean b = quanZiApi.likeComment(userId, publishId);
        System.out.println(b);
        Boolean data = this.quanZiApi.UserIsLike(userId, publishId);
        System.out.println(data);

        Long likeCount2 = this.quanZiApi.getLikeCount(publishId);
        System.out.println(likeCount2);
        boolean b1 = this.quanZiApi.removeLikeComment(userId, publishId);
        System.out.println(b1);
        Long likeCount3 = this.quanZiApi.getLikeCount(publishId);
        System.out.println(likeCount3);
    }
}
