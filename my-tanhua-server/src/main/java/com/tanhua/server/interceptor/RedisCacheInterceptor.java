package com.tanhua.server.interceptor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanhua.server.util.Cache;
import com.tanhua.server.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Key;
import java.util.Map;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/14
 * @描述
 */
@Component
@Slf4j
public class RedisCacheInterceptor implements HandlerInterceptor {

    private static final ObjectMapper MAPPER = new ObjectMapper();
    @Value("${tanhua.cache.enable}")
    private boolean enable;
    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //缓存的全局开关的校验
        if (!enable) {
            return true;
        }
        //校验handler是否是HandlerMethod
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        //判断是否为get请求
        HandlerMethod handlerMethod= (HandlerMethod) handler;
        if (!handlerMethod.hasMethodAnnotation(GetMapping.class)) {
            return true;
        }
        //判断是否添加了@Cache注解
        if (!handlerMethod.hasMethodAnnotation(Cache.class)) {
            return true;
        }
        //获取缓存
        String redisKey = getRedisKey(request);
        String cacheData = this.redisTemplate.opsForValue().get(redisKey);
        //缓存未命中
        if(StringUtils.isEmpty(cacheData)){
            //缓存未命中
            log.debug("缓存未命中,URL："+request.getRequestURL());
            return true;
        }
        // 将data数据进行响应
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        response.getWriter().write(cacheData);
        log.debug("缓存已命中,URL："+request.getRequestURL());
        return false;
    }
    /**
     * 生成redis中的key，规则：SERVER_CACHE_DATA_MD5(url + param + token)
     *
     * @param request
     * @return
     */
    public String getRedisKey(HttpServletRequest request) throws JsonProcessingException {
        String uri = request.getRequestURI();
        Map<String, String[]> parameterMap = request.getParameterMap();
        String param = MAPPER.writeValueAsString(parameterMap);
        String token = request.getHeader("Authorization");
        String key = uri + param + token;
        return "SERVER_CACHE_DATA_MD5" + DigestUtils.md5Hex(key);


    }
}
