package com.tanhua.server.interceptor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanhua.server.util.Cache;
import com.tanhua.server.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.AnnotatedElement;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/14
 * @描述 ResponseBodyAdvice:相当于在给前端返回数据之前，拦截下来进行对数据的操作。
 * 此方法用于对查询用户的结果进行redis缓存。
 */

@ControllerAdvice
public class RedisCacheResponseBodyAdvice implements ResponseBodyAdvice {

    private static final ObjectMapper MAPPER = new ObjectMapper();
    @Value("${tanhua.cache.enable}")
    private Boolean enable;
    @Autowired
    private RedisCacheInterceptor redisCacheInterceptor;


    @Autowired
    private RedisUtil redisUtil;

    @Override
    /**
     *@描述 supports ：用于 判断是否进行拦截。
     *@参数 [methodParameter 方法相关的参数获取, aClass  类相关的参数获取]
     *@返回值 boolean 是否拦截 true为拦截
     *@创建人 1anma0
     *@创建时间 2021/7/14
     *@修改人和其它信息
     */
    public boolean supports(MethodParameter methodParameter, Class aClass) {
        // 1开关处于开启状态  2是get请求  3包含了@Cache注解
        boolean b = enable && methodParameter.hasMethodAnnotation(Cache.class) && methodParameter.hasMethodAnnotation(GetMapping.class);
        return b;

    }

    @Override
    /**
     *@描述 beforeBodyWrite 在拦截的数据进行操作的逻辑判断方法
     *@参数 [o 返回的类容体, methodParameter：方法相关的参数获取, mediaType：????, aClass：类相关的参数,
     * serverHttpRequest：请求相关的参数获取记得转化为HttpServletRequest，, serverHttpResponse：响应体相关的参数获取]
     *@返回值 java.lang.Object 一般还是返回O
     *@创建人 1anma0
     *@创建时间 2021/7/14
     *@修改人和其它信息
     */
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {

        if (null == o) {
            return null;
        }
        String Value = null;
        try {
//判断返回值的类型是否为String
            if (o instanceof String) {
                Value = (String) o;
            } else {
                Value = MAPPER.writeValueAsString(o);
            }
            String rediskey = redisCacheInterceptor.getRedisKey(((ServletServerHttpRequest) serverHttpRequest).getServletRequest());
            Cache cache = methodParameter.getMethodAnnotation(Cache.class);

            redisUtil.set(rediskey, Value, cache.time());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return o;
    }
}
