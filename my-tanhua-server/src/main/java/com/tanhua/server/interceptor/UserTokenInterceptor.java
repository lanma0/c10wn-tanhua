package com.tanhua.server.interceptor;

import cn.hutool.core.util.StrUtil;
import com.tanhua.common.pojo.User;
import com.tanhua.common.util.UserThreadLocal;
import com.tanhua.server.Service.UserService;
import com.tanhua.server.util.NoAuthorization;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/15
 * @描述
 */
@Component
@Slf4j
public class UserTokenInterceptor implements HandlerInterceptor {


    @Autowired
    private UserService userService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        StringBuffer requestURL = request.getRequestURL();

        //校验handler是否是HandlerMethod
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        //判断是否包含@NoAuthorization注解，如果包含，直接放行
        if (((HandlerMethod) handler).hasMethodAnnotation(NoAuthorization.class)) {
            return true;
        }


        //3.获得请求头的token
        String token = request.getHeader("Authorization");

        if (StringUtils.isNotEmpty(token)) {

            //将结果发送验证。
            User user = userService.chekToken(token);
            if (user != null) {
                //存入本地线程中!!!
                //将放回值放到LocalThead中
                UserThreadLocal.set(user);
                log.info(requestURL+"");
                return true;
            }
        }

        response.setStatus(401);//无权限

        return false;


    }


    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

        UserThreadLocal.remove();
    }
}
