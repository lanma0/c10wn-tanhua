package com.tanhua.server.controller;

import com.tanhua.server.Service.QuanZiService;
import com.tanhua.server.pojo.TodayBest;
import com.tanhua.server.vo.PageResult;
import com.tanhua.server.vo.QuanZiVo;
import com.tanhua.server.vo.VisitorsVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.List;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/14
 * @描述
 */
@RestController
@RequestMapping("movements")
@Slf4j
public class QuanZiController {

    @Autowired
    private QuanZiService quanZiService;


    @GetMapping
    public ResponseEntity<Object> queryPublishList(@RequestParam(name = "page", defaultValue = "1") Integer page,
                                                   @RequestParam(name = "pagesize", defaultValue = "10") Integer pagesize) {


        try {
            PageResult pageResult = quanZiService.queryPublishList(page, pagesize);
            if (null != pageResult) {
                return ResponseEntity.ok(pageResult);
            }
        } catch (Exception e) {
            log.error("查询今日佳人出错~ token = " , e);
        }

        return ResponseEntity.status(500).body(null);

    }

    @PostMapping
    public ResponseEntity<Void> savePublish(@RequestParam("textContent") String textContent,
                                            @RequestParam(value = "location", required = false) String location,
                                            @RequestParam(value = "latitude", required = false) String latitude,
                                            @RequestParam(value = "longitude", required = false) String longitude,
                                            @RequestParam(value = "imageContent", required = false) MultipartFile[] multipartFile) {

        try {
            String pulishId = quanZiService.savePublish(textContent, location, latitude, longitude, multipartFile);
            if (StringUtils.isNotEmpty(pulishId)) {
                return ResponseEntity.ok(null);

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return ResponseEntity.status(500).body(null);

    }


    @GetMapping("/recommend")
    public ResponseEntity<Object> queryRecommendPublishList(@RequestParam(name = "page", defaultValue = "1") Integer page,
                                                   @RequestParam(name = "pagesize", defaultValue = "10") Integer pagesize,
                                                   @RequestHeader("Authorization") String token) {


        try {
            PageResult pageResult = quanZiService.queryRecommendPublishList(page, pagesize);
            if (null != pageResult) {
                return ResponseEntity.ok(pageResult);
            }
        } catch (Exception e) {
            log.error("查询今日佳人出错~ token = " + token, e);
        }

        return ResponseEntity.status(500).body(null);

    }


//    @GetMapping("/all")
//    public ResponseEntity<Object> queryUserPublishList(@RequestParam(name = "page", defaultValue = "1") Integer page,
//                                                            @RequestParam(name = "pagesize", defaultValue = "10") Integer pagesize,
//                                                       @RequestParam(name = "userId") Long userId
//                                                            ) {
//
//
//        try {
//            PageResult pageResult = quanZiService.queryUserPublishList(userId,page, pagesize);
//            if (null != pageResult) {
//                return ResponseEntity.ok(pageResult);
//            }
//        } catch (Exception e) {
//            log.error("查询今日佳人出错~ token = " , e);
//        }
//
//        return ResponseEntity.status(500).body(null);
//
//    }



    @GetMapping("/{id}/like")
    public ResponseEntity<Long> likeComment(@PathVariable("id") String publishId) {
        try {
            Long likeCount = this.quanZiService.likeComment(publishId);
            if (likeCount != null) {
                return ResponseEntity.ok(likeCount);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 取消点赞
     *
     * @param publishId
     * @return
     */
    @GetMapping("/{id}/dislike")
    public ResponseEntity<Long> disLikeComment(@PathVariable("id") String publishId) {
        try {
            Long likeCount = this.quanZiService.disLikeComment(publishId);
            if (null != likeCount) {
                return ResponseEntity.ok(likeCount);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }


    @GetMapping("/{id}/love")
    public ResponseEntity<Long> loveComment(@PathVariable("id") String publishId) {
        try {
            Long likeCount = this.quanZiService.loveComment(publishId);
            if (likeCount != null) {
                return ResponseEntity.ok(likeCount);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 取消点赞
     *
     * @param publishId
     * @return
     */
    @GetMapping("/{id}/unlove")
    public ResponseEntity<Long> disLoveComment(@PathVariable("id") String publishId) {
        try {
            Long likeCount = this.quanZiService.disLoveComment(publishId);
            if (null != likeCount) {
                return ResponseEntity.ok(likeCount);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<QuanZiVo> queryById(@PathVariable("id") String publishId) {
        try {
            QuanZiVo movements = this.quanZiService.queryById(publishId);
            if(null != movements){
                return ResponseEntity.ok(movements);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * TODO：谁看过我
     *
     * @return
     */
    @GetMapping("visitors")
    public ResponseEntity<Object> queryVisitors() {
        return ResponseEntity.ok(Collections.EMPTY_LIST);
    }


    /**
     * 自己的所有动态
     *
     * @return
     */
    @GetMapping("all")
    public ResponseEntity<PageResult> queryAlbumList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                     @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize,
                                                     @RequestParam(value = "userId") Long userId) {
        try {
            PageResult pageResult = this.quanZiService.queryAlbumList(userId, page, pageSize);
            return ResponseEntity.ok(pageResult);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 谁看过我
     *
     * @return
     */
    @GetMapping("visitors")
    public ResponseEntity<List<VisitorsVo>> queryVisitorsList(){
        try {
            List<VisitorsVo> list = this.quanZiService.queryVisitorsList();
            return ResponseEntity.ok(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
