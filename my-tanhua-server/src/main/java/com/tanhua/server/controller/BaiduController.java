package com.tanhua.server.controller;

import com.tanhua.server.Service.BaiduService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/22
 * @描述
 */


@RestController
@RequestMapping("baidu")
public class BaiduController {
    @Autowired
    private BaiduService baiduService;


    public ResponseEntity<Void> updateLocation(@RequestBody Map<String, Object> param) {
            //1.拿到参数
        try {
            Double longitude = Double.valueOf(param.get("longitude").toString());
            Double latitude = Double.valueOf(param.get("latitude").toString());
            String address = param.get("address").toString();

            Boolean bool = this.baiduService.updateLocation(longitude, latitude, address);
            if (bool){
                return ResponseEntity.ok(null);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}