package com.tanhua.server.controller;

import com.tanhua.server.Service.QuanZiService;
import com.tanhua.server.vo.CommentVo;
import com.tanhua.server.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/18
 * @描述
 */

@RestController
@RequestMapping("comments")
public class QuanziCommentController {

    @Autowired
    private QuanZiService quanZiService;
    @GetMapping
    public ResponseEntity queryCommentList(@RequestParam("movementId") String publishId,
                                           @RequestParam(value = "page", defaultValue = "1") Integer page,
                                           @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize){

        try {
            PageResult pageResult = quanZiService.queryCommentList(page, pageSize, publishId);
            if (pageResult!=null) {
                return ResponseEntity.ok(pageResult);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(500).build();
    }
    /**
     * 保存评论
     */

    @PostMapping
    public ResponseEntity<Void> saveComments(@RequestBody Map<String, String> param) {
        try {
            String publishId = param.get("movementId");
            String content = param.get("comment");
            Boolean result = this.quanZiService.saveComment(publishId, content);
            if (result) {
                return ResponseEntity.ok(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 点赞
     *
     * @param commentId
     * @return
     */
    @GetMapping("{id}/like")
    public ResponseEntity<Long> likeComment(@PathVariable("id") String commentId) {
        try {
            Long likeCount = this.quanZiService.likeComment(commentId);
            if (likeCount != null) {
                return ResponseEntity.ok(likeCount);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 取消点赞
     *
     * @param commentId
     * @return
     */
   @GetMapping("{id}/dislike")
    public ResponseEntity<Long> disLikeComment(@PathVariable("id") String commentId) {
        try {
            Long likeCount = this.quanZiService.disLikeComment(commentId);
            if (null != likeCount) {
                return ResponseEntity.ok(likeCount);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

}
