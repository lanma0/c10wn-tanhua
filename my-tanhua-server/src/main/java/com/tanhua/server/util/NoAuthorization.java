package com.tanhua.server.util;

import java.lang.annotation.*;

/**
 * @author 86138
 * @创建人 1anma0
 * @创建时间 2021/7/14
 * @描述 用来标记是否需要token校验,如果加了就是不需要.
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NoAuthorization {



}
