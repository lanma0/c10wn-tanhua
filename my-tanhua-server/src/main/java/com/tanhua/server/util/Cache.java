package com.tanhua.server.util;

import java.lang.annotation.*;

/**
 * @author 86138
 * @创建人 1anma0
 * @创建时间 2021/7/14
 * @描述
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Cache {

    int time() default 60;


}
