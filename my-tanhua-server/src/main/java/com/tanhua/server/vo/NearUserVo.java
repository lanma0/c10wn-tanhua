package com.tanhua.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
//附近人的返回值.
public class NearUserVo {

    private Long userId;
    private String avatar;
    private String nickname;

}