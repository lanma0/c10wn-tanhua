package com.tanhua.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/12
 * @描述
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecommendUserQueryParam {

    private Integer page = 1; //当前页数
    private Integer pagesize = 10; //页尺寸
    private String gender; //性别 man woman
    private String lastLogin; //近期登陆时间
    private Integer ageMax; //年龄
    private Integer ageMin; //年龄
    private String city; //居住地
    private String education; //学历
}
