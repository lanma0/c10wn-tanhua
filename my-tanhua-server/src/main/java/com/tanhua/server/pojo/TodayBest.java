package com.tanhua.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/12
 * @描述
 */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public class TodayBest {

        private Long id;
        private String avatar;
        private String nickname;
        private String gender; //性别 man woman
        private Integer age;
        private String[] tags;
        private Long fateValue; //缘分值

}
