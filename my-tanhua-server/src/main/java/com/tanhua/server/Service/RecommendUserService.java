package com.tanhua.server.Service;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.dubbo.server.api.RecommendUserApi;
import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.RecommendUser;
import com.tanhua.server.pojo.TodayBest;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @创建人 1anma0
 * @创建时间 2021/7/12
 * @描述
 */

@Service
@Slf4j
public class RecommendUserService {

    @Reference(version = "1.0.0")
    private RecommendUserApi recommendUserApi;



    public TodayBest earnRecommendUser(Long userId) {

        RecommendUser recommendUser = recommendUserApi.getMaxScoreUser(userId);
        if (null == recommendUser) {
            //给出默认的推荐用户
            return null;
        }
        TodayBest todayBest = new TodayBest();
        todayBest.setId(recommendUser.getUserId());
        //将缘分值调整为整数，然然后加到返回值中
        Double score = recommendUser.getScore();
        double floor = Math.floor(score);
        todayBest.setFateValue(Double.valueOf(floor).longValue());
        return todayBest;

    }

    public PageInfo<RecommendUser> queryRecommendUserList(Long userId, Integer pageNum, Integer pageSize) {

        PageInfo<RecommendUser> userPageInfo = null;
        try {
            userPageInfo = recommendUserApi.getUserPageInfo(userId, pageNum, pageSize);
        } catch (Exception e) {
            log.error("推荐用户列表查询失败："+e.getMessage());
            return null;
        }

        return userPageInfo;
    }

    public Double queryScore(Long userId, Long toUserId){
        Double score = this.recommendUserApi.queryScore(userId, toUserId);
        if(ObjectUtil.isNotEmpty(score)){
            return score;
        }
        //默认值
        return 98d;
    }

    public List<RecommendUser> queryCardList(Long userId, Integer count){
        return recommendUserApi.queryCardList(userId,count);
    }


}
