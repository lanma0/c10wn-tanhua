package com.tanhua.server.Service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.github.tobato.fastdfs.domain.conn.FdfsWebServer;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.tanhua.common.pojo.User;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.common.service.BuddhaLoadService;
import com.tanhua.common.util.UserThreadLocal;
import com.tanhua.common.vo.BuddhaLoadResoult;
import com.tanhua.dubbo.server.api.QuanZiApi;
import com.tanhua.dubbo.server.api.VideoApi;
import com.tanhua.dubbo.server.pojo.Comment;
import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.Video;
import com.tanhua.server.vo.PageResult;
import com.tanhua.server.vo.VideoVo;
import org.apache.commons.io.FileUtils;
import org.omg.PortableInterceptor.Interceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.aggregation.ArrayOperators;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/18
 * @描述
 */
@Service
public class VideoService {
    @Autowired
    protected FastFileStorageClient storageClient;
    @Reference(version = "1.0.0")
    private VideoApi videoApi;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private FdfsWebServer fdfsWebServer;

    @Autowired
    private BuddhaLoadService buddhaLoadService;
    @Reference(version = "1.0.0")
    private QuanZiApi quanZiApi;

    @Autowired
    private QuanZiService quanZiService;


    public boolean saveVideo(MultipartFile picFile, MultipartFile videoFile) {
        User user = UserThreadLocal.get();
        Long id = user.getId();
        Video video = new Video();
        video.setUserId(id);
        video.setSeeType(1);


        try {

            //将视频和
            StorePath storePath = this.storageClient.uploadFile(videoFile.getInputStream(), videoFile.getSize(), StrUtil.subAfter(videoFile.getOriginalFilename(), ".", true), null);
            BuddhaLoadResoult buddhaLoadResoult = buddhaLoadService.buddhaLoad(picFile);

            video.setPicUrl(buddhaLoadResoult.getBName());
            video.setVideoUrl(fdfsWebServer.getWebServerUrl() + storePath.getFullPath());

            String videoId = videoApi.SaveVideo(video);
            return StrUtil.isNotEmpty(videoId);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public PageResult queryVideoList(Integer page, Integer pageSize) {
        User user = UserThreadLocal.get();
        PageResult pageResult = new PageResult();
        pageResult.setPage(page);
        pageResult.setPagesize(pageSize);
        PageInfo<Video> videoPageInfo = videoApi.queryVideoList(user.getId(), page, pageSize);
        List<Video> recommendUser = videoPageInfo.getRecommendUser();
        if (CollUtil.isEmpty(recommendUser)) {
            return pageResult;
        }

        //获得用户基本信息相关
        List<Object> userIds = CollUtil.getFieldValues(recommendUser, "userId");
        List<UserInfo> userInfos = userInfoService.getUserInfos(userIds);

        List<VideoVo> videoVos = new ArrayList<>();
        for (Video video : recommendUser) {
            VideoVo videoVo = new VideoVo();
            videoVo.setVideoUrl(video.getVideoUrl());
            videoVo.setSignature("我就是我~"); //TODO 签名
            videoVo.setUserId(video.getUserId());
            videoVo.setId(video.getId().toHexString());
            videoVo.setCover(video.getPicUrl());

            videoVo.setCommentCount(Convert.toInt(quanZiApi.getCommentCount(videoVo.getId()))); //TODO 评论数
            videoVo.setHasFocus(videoApi.isFollowUser(user.getId(),videoVo.getUserId())?1:0); //TODO 是否关注
            videoVo.setHasLiked(quanZiApi.UserIsLike(videoVo.getUserId(),videoVo.getId())?1:0); //TODO 是否点赞（1是，0否）
            videoVo.setLikeCount(Convert.toInt(quanZiApi.getLikeCount(videoVo.getId())));//TODO 点赞数

            for (UserInfo userInfo : userInfos) {
                if (ObjectUtil.equals(userInfo.getUserId(), video.getUserId())) {
                    //将基本信息注入进来。
                    videoVo.setNickname(userInfo.getNickName());
                    videoVo.setAvatar(userInfo.getLogo());
                    break;
                }
            }
            videoVos.add(videoVo);
        }
        //将结果对象对象注入返回结果。
        pageResult.setItems(videoVos);

        return pageResult;


    }

    public Long likeComment(String videoId) {
        User user = UserThreadLocal.get();
        boolean b = quanZiApi.likeComment(user.getId(), videoId);
        if (b) {
            return quanZiApi.getLikeCount(videoId);
        }
        return null;

    }

    public Long disLikeComment(String videoId) {
        User user = UserThreadLocal.get();
        boolean b = quanZiApi.removeLikeComment(user.getId(), videoId);
        if (b) {
            return quanZiApi.getLikeCount(videoId);
        }
        return null;

    }

    public PageResult queryCommentList(String videoId, Integer page, Integer pageSize) {
        PageResult pageResult = quanZiService.queryCommentList(page,pageSize, videoId);
        return pageResult;
    }

    public Boolean saveComment(String videoId, String content) {
        boolean saveComment = quanZiService.saveComment(videoId, content);
        return saveComment;
    }

    //这是一个关注.
    public Boolean followUser(Long userId) {
        User user = UserThreadLocal.get();
        Boolean aBoolean = videoApi.followUser(user.getId(), userId);
        return aBoolean;

    }
    //这是视频取消关注
    public Boolean disFollowUser(Long userId) {
        User user = UserThreadLocal.get();
        Boolean aBoolean = videoApi.removeFollowUser(user.getId(), userId);
        return aBoolean;
    }
}