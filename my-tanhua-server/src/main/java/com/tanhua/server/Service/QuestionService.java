package com.tanhua.server.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.common.mapper.QuestionMapper;
import com.tanhua.common.pojo.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/23
 * @描述
 */
@Service
public class QuestionService {

    @Autowired
    private QuestionMapper questionMapper;

    public Question queryQuestion(Long userId) {

        QueryWrapper<Question> questionQueryWrapper = new QueryWrapper<>();
        questionQueryWrapper.eq("user_id", userId);
        Question question = questionMapper.selectOne(questionQueryWrapper);

        return question;

    }

}
