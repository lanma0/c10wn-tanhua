package com.tanhua.server.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanhua.common.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/12
 * @描述
 */

@Service
@Slf4j
public class UserService {


    @Value("${tanhua.sso.tokenUrl}")
    private String tokenUrl;

    @Autowired
    private RestTemplate restTemplate;

    private static final ObjectMapper MAPPER =new ObjectMapper();

    public User chekToken(String token){
        String url = tokenUrl + "/user/" + token;

        try {
            String userStr = restTemplate.getForObject(url, String.class);
            return MAPPER.readValue(userStr, User.class);
        } catch (IOException e) {
            log.error("校验token出错，token = " + token, e);
        }
        return null;

    }


}
