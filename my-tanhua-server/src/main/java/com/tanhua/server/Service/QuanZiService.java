package com.tanhua.server.Service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.unit.DataUnit;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.common.pojo.User;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.common.service.BuddhaLoadService;
import com.tanhua.common.util.UserThreadLocal;
import com.tanhua.common.vo.BuddhaLoadResoult;
import com.tanhua.dubbo.server.api.QuanZiApi;
import com.tanhua.dubbo.server.api.VisitorsApi;
import com.tanhua.dubbo.server.pojo.Comment;
import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.Publish;
import com.tanhua.dubbo.server.pojo.Visitors;
import com.tanhua.server.util.RelativeDateFormat;
import com.tanhua.server.vo.CommentVo;
import com.tanhua.server.vo.PageResult;
import com.tanhua.server.vo.QuanZiVo;
import com.tanhua.server.vo.VisitorsVo;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.lang.model.element.VariableElement;
import java.util.*;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/14
 * @描述
 */
@Service
public class QuanZiService {

    @Autowired
    private UserService userService;
    @Reference(version = "1.0.0")
    private QuanZiApi quanZiApi;
    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private BuddhaLoadService buddhaLoadService;
    @Reference
    private VisitorsApi visitorsApi;

    public PageResult queryPublishList(Integer page, Integer pagesize) {
        PageResult pageResult = new PageResult();
        pageResult.setPage(page);
        pageResult.setPagesize(pagesize);

        User user = UserThreadLocal.get();
        //------1.查找pulish中的数据，封装到结果中--------------
        PageInfo<Publish> pageInfo = quanZiApi.queryPublishList(page, pagesize, user.getId());

        if (null == pageInfo) {
            return pageResult;
        }


        List<QuanZiVo> quanZiVos = new ArrayList<>();
        List<Publish> publishes = pageInfo.getRecommendUser();
        publishes.forEach(publish -> {
            QuanZiVo quanZiVo = new QuanZiVo();
            //将pulish中的数据，封装到quanziVo中。

//            private String id; //动态id
            quanZiVo.setId(publish.getId().toHexString());
            quanZiVo.setTextContent(publish.getText());
            quanZiVo.setImageContent(publish.getMedias().toArray(new String[]{}));
            quanZiVo.setCreateDate(RelativeDateFormat.format(new Date(publish.getCreated())));

            //距离数。
            quanZiVo.setDistance(publish.getLocationName());
//
//
//            private Long userId; //用户id
            quanZiVo.setUserId(publish.getUserId());

            //不能用这种编列查询数据库的方法，对数据库的压力太大。
//            UserInfo userInfo = userInfoService.getUserInfo(publish.getUserId());
//            quanZiVo.setAge(userInfo.getAge());
//            quanZiVo.setNickname(userInfo.getNickName());
//            quanZiVo.setTags(StringUtils.split(userInfo.getTags(),","));
//            quanZiVo.setGender(userInfo.getSex()==1?"men":"women");
//            quanZiVo.setAvatar(userInfo.getLogo());
//            fillUserInfoToQuanZiVo(userInfo,quanZiVo);
            //将每一个封装到集合中
            quanZiVos.add(quanZiVo);
        });

        List<Object> userId = CollUtil.getFieldValues(publishes, "userId");
        List<UserInfo> userInfos = userInfoService.getUserInfos(userId);

        for (QuanZiVo quanZiVo : quanZiVos) {
            for (UserInfo userInfo : userInfos) {
                if (quanZiVo.getUserId().longValue() == userInfo.getUserId().longValue()) {
                    fillUserInfoToQuanZiVo(userInfo, quanZiVo);
                    break;
                }
            }
        }
        pageResult.setItems(quanZiVos);
        return pageResult;

    }
    //-------2.查找基本信息userInfo，封装到结果中

    private void fillUserInfoToQuanZiVo(UserInfo userInfo, QuanZiVo quanZiVo) {

        quanZiVo.setAge(userInfo.getAge());
        quanZiVo.setNickname(userInfo.getNickName());
        quanZiVo.setTags(StringUtils.split(userInfo.getTags(), ","));
        quanZiVo.setGender(userInfo.getSex() == 1 ? "men" : "women");
        quanZiVo.setAvatar(userInfo.getLogo());

        User user = UserThreadLocal.get();

        quanZiVo.setCommentCount(Convert.toInt(quanZiApi.getCommentCount(quanZiVo.getId()))); //TODO 评论数
        quanZiVo.setDistance("1.2公里"); //TODO 距离
        quanZiVo.setHasLiked(quanZiApi.UserIsLike(user.getId(), quanZiVo.getId()) ? 1 : 0); //TODO 是否点赞（1是，0否）
        quanZiVo.setLikeCount(Convert.toInt(quanZiApi.getLikeCount(quanZiVo.getId()))); //TODO 点赞数
        quanZiVo.setHasLoved(quanZiApi.UserIsLove(user.getId(), quanZiVo.getId()) ? 1 : 0); //TODO 是否喜欢（1是，0否）
        quanZiVo.setLoveCount(Convert.toInt(quanZiApi.getLoveCount(quanZiVo.getId()))); //TODO 喜欢数
    }

    public String savePublish(String textContent, String location, String latitude, String longitude, MultipartFile[] multipartFile) {
        User user = UserThreadLocal.get();

        Publish publish = new Publish();
        publish.setUserId(user.getId());
        publish.setText(textContent);
        publish.setLocationName(location);
        publish.setLatitude(latitude);
        publish.setLongitude(longitude);
        publish.setSeeType(1);//这里不做功能,只要前端传数据过来就行的.
        List<String> fileNames = new ArrayList<>();

        for (MultipartFile file : multipartFile) {
            BuddhaLoadResoult buddhaLoadResoult = buddhaLoadService.buddhaLoad(file);
            String bName = buddhaLoadResoult.getBName();
            fileNames.add(bName);
        }
        publish.setMedias(fileNames);
        String s = quanZiApi.savePublish(publish);
        return s;
    }

    public PageResult queryRecommendPublishList(Integer page, Integer pagesize) {

        PageResult pageResult = new PageResult();
        pageResult.setPage(page);
        pageResult.setPagesize(pagesize);

        User user = UserThreadLocal.get();
        //------1.查找pulish中的数据，封装到结果中--------------
        PageInfo<Publish> pageInfo = quanZiApi.queryRecommendPublishList(page, pagesize, user.getId());

        if (null == pageInfo) {
            return pageResult;
        }


        List<Publish> publishes = pageInfo.getRecommendUser();
        List<QuanZiVo> quanZiVos = getQuanZiVos(publishes);
        pageResult.setItems(quanZiVos);
        return pageResult;

    }

    private List<QuanZiVo> getQuanZiVos(List<Publish> publishes) {
        List<QuanZiVo> quanZiVos = new ArrayList<>();
        publishes.forEach(publish -> {
            QuanZiVo quanZiVo = new QuanZiVo();
            //将pulish中的数据，封装到quanziVo中。

//            private String id; //动态id
            quanZiVo.setId(publish.getId().toHexString());
            quanZiVo.setTextContent(publish.getText());
            quanZiVo.setImageContent(publish.getMedias().toArray(new String[]{}));
            quanZiVo.setCreateDate(RelativeDateFormat.format(new Date(publish.getCreated())));

            //距离数。
            quanZiVo.setDistance(publish.getLocationName());
//
//
//            private Long userId; //用户id
            quanZiVo.setUserId(publish.getUserId());

            //不能用这种编列查询数据库的方法，对数据库的压力太大。
//            UserInfo userInfo = userInfoService.getUserInfo(publish.getUserId());
//            quanZiVo.setAge(userInfo.getAge());
//            quanZiVo.setNickname(userInfo.getNickName());
//            quanZiVo.setTags(StringUtils.split(userInfo.getTags(),","));
//            quanZiVo.setGender(userInfo.getSex()==1?"men":"women");
//            quanZiVo.setAvatar(userInfo.getLogo());
//            fillUserInfoToQuanZiVo(userInfo,quanZiVo);
            //将每一个封装到集合中
            quanZiVos.add(quanZiVo);
        });

        List<Object> userId = CollUtil.getFieldValues(publishes, "userId");
        List<UserInfo> userInfos = userInfoService.getUserInfos(userId);

        for (QuanZiVo quanZiVo : quanZiVos) {
            for (UserInfo userInfo : userInfos) {
                if (quanZiVo.getUserId().longValue() == userInfo.getUserId().longValue()) {
                    fillUserInfoToQuanZiVo(userInfo, quanZiVo);
                    break;
                }
            }
        }
        return quanZiVos;
    }

    public Long likeComment(String publishId) {
        User user = UserThreadLocal.get();
        //先进行点赞操作。
        boolean b = quanZiApi.likeComment(user.getId(), publishId);
        //将点赞后的数据反馈到前端.
        if (b) {
            Long likeCount = quanZiApi.getLikeCount(publishId);
            return likeCount;
        }

        return null;

    }

    public Long disLikeComment(String publishId) {
        User user = UserThreadLocal.get();
        //先进行点赞操作。
        boolean b = quanZiApi.removeLikeComment(user.getId(), publishId);
        //将点赞后的数据反馈到前端.
        if (b) {
            Long likeCount = quanZiApi.getLikeCount(publishId);
            return likeCount;
        }

        return null;


    }

    public Long loveComment(String publishId) {

        User user = UserThreadLocal.get();
        //先进行点赞操作。
        boolean b = quanZiApi.loveComment(user.getId(), publishId);
        //将点赞后的数据反馈到前端.
        if (b) {
            Long loveCount = quanZiApi.getLoveCount(publishId);
            return loveCount;
        }

        return null;
    }

    public Long disLoveComment(String publishId) {

        User user = UserThreadLocal.get();
        //先进行点赞操作。
        boolean b = quanZiApi.removeLoveComment(user.getId(), publishId);
        //将点赞后的数据反馈到前端.
        if (b) {
            Long loveCount = quanZiApi.getLoveCount(publishId);
            return loveCount;
        }

        return null;
    }

    public QuanZiVo queryById(String publishId) {
        User user = UserThreadLocal.get();
        Publish publish = quanZiApi.queryPublishById(publishId);
        if (publish == null) {
            return null;
        }
        return getQuanZiVos(Arrays.asList(publish)).get(0);

    }

    public PageResult queryCommentList(Integer page, Integer pageSize, String publishId) {
        User user = UserThreadLocal.get();
        PageResult pageResult = new PageResult();
        pageResult.setPage(page);
        pageResult.setPagesize(pageSize);
        PageInfo<Comment> commentPageInfo = quanZiApi.queryCommentList(publishId, page, pageSize);

        if (commentPageInfo == null) {
            return null;
        }
        List<Comment> comments = commentPageInfo.getRecommendUser();
        if (CollUtil.isEmpty(comments)) {
            return pageResult;
        }

        List<CommentVo> commentVos = new ArrayList<>();
        List<Object> userIds = CollUtil.getFieldValues(comments, "userId");
        List<UserInfo> userInfos = userInfoService.getUserInfos(userIds);
        for (Comment comment : comments) {
            CommentVo commentVo = new CommentVo();
            commentVo.setContent(comment.getContent());
            commentVo.setCreateDate(DateUtil.format(new Date(comment.getCreated()), "HH:mm"));
            commentVo.setId(comment.getId().toHexString());


            for (UserInfo userInfo : userInfos) {
                if (ObjectUtil.equals(userInfo.getUserId(), comment.getUserId())) {
                    commentVo.setAvatar(userInfo.getLogo());
                    commentVo.setNickname(userInfo.getNickName());
                    break;
                }

            }
            commentVo.setHasLiked(quanZiApi.UserIsLike(user.getId(), comment.getId().toHexString()) ? 1 : 0);
            commentVo.setLikeCount(Convert.toInt(quanZiApi.getLikeCount(comment.getId().toHexString())));
            commentVos.add(commentVo);

        }
        pageResult.setItems(commentVos);
        return pageResult;

    }

    public boolean saveComment(String pulishId, String content) {
        User user = UserThreadLocal.get();
        Boolean aBoolean = quanZiApi.saveComment(pulishId, user.getId(), content);

        return aBoolean;


    }

    public PageResult queryUserPublishList(Long userId, Integer page, Integer pagesize) {


        PageResult pageResult = new PageResult();
        pageResult.setPage(page);
        pageResult.setPagesize(pagesize);

        User user = UserThreadLocal.get();
        //------1.查找pulish中的数据，封装到结果中--------------
        PageInfo<Publish> pageInfo = quanZiApi.queryPublishList(page, pagesize, userId);

        if (null == pageInfo) {
            return pageResult;
        }


        List<Publish> publishes = pageInfo.getRecommendUser();
        List<QuanZiVo> quanZiVos = getQuanZiVos(publishes);
        pageResult.setItems(quanZiVos);
        return pageResult;
    }


    public PageResult queryAlbumList(Long userId, Integer page, Integer pageSize) {
        PageResult pageResult = new PageResult();
        pageResult.setPage(page);
        pageResult.setPagesize(pageSize);

        //查询数据
        PageInfo<Publish> pageInfo = this.quanZiApi.queryAlbumList(userId, page, pageSize);
        if (CollUtil.isEmpty(pageInfo.getRecommendUser())) {
            return pageResult;
        }

        List<Publish> publishes = pageInfo.getRecommendUser();
        List<QuanZiVo> quanZiVos = new ArrayList<>();
        publishes.forEach(publish -> {
            QuanZiVo quanZiVo = new QuanZiVo();
            //将pulish中的数据，封装到quanziVo中。

//            private String id; //动态id
            quanZiVo.setId(publish.getId().toHexString());
            quanZiVo.setTextContent(publish.getText());
            quanZiVo.setImageContent(publish.getMedias().toArray(new String[]{}));
            quanZiVo.setCreateDate(RelativeDateFormat.format(new Date(publish.getCreated())));

            //距离数。
            quanZiVo.setDistance(publish.getLocationName());
            quanZiVo.setUserId(publish.getUserId());
            quanZiVos.add(quanZiVo);
        });

        List<Object> userIds = CollUtil.getFieldValues(publishes, "userId");
        List<UserInfo> userInfos = userInfoService.getUserInfos(userIds);

        for (QuanZiVo quanZiVo : quanZiVos) {
            for (UserInfo userInfo : userInfos) {
                if (quanZiVo.getUserId().longValue() == userInfo.getUserId().longValue()) {
                    fillUserInfoToQuanZiVo(userInfo, quanZiVo);
                    break;
                }
            }
        }
        pageResult.setItems(quanZiVos);
        return pageResult;

    }

    public List<VisitorsVo> queryVisitorsList() {

        User user = UserThreadLocal.get();
        List<Visitors> visitorsList = this.visitorsApi.queryMyVisitor(user.getId());
        if (CollUtil.isEmpty(visitorsList)) {
            return Collections.emptyList();
        }

        List<Object> userIds = CollUtil.getFieldValues(visitorsList, "visitorUserId");
        List<UserInfo> userInfoList = userInfoService.getUserInfos(userIds);

        List<VisitorsVo> visitorsVoList = new ArrayList<>();
        for (Visitors visitor : visitorsList) {
            for (UserInfo userInfo : userInfoList) {
                if (ObjectUtil.equals(visitor.getVisitorUserId(), userInfo.getUserId())) {

                    VisitorsVo visitorsVo = new VisitorsVo();
                    visitorsVo.setAge(userInfo.getAge());
                    visitorsVo.setAvatar(userInfo.getLogo());
                    visitorsVo.setGender(userInfo.getSex() == 1 ? "men" : "women");
                    visitorsVo.setId(userInfo.getUserId());
                    visitorsVo.setNickname(userInfo.getNickName());
                    visitorsVo.setTags(StringUtils.split(userInfo.getTags(), ','));
                    visitorsVo.setFateValue(visitor.getScore().intValue());

                    visitorsVoList.add(visitorsVo);
                    break;
                }
            }


        }
        return visitorsVoList;
    }


}
