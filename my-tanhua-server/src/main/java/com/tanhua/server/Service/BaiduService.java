package com.tanhua.server.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.common.pojo.User;
import com.tanhua.common.util.UserThreadLocal;
import com.tanhua.dubbo.server.api.UserLocationApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/22
 * @描述
 */


@Service
@Slf4j
public class BaiduService {

    @Reference(version = "1.0.0")
    private UserLocationApi userLocationApi;


    public Boolean updateLocation(Double longitude, Double latitude, String address) {
        User user = UserThreadLocal.get();
        try {
            Boolean aBoolean = userLocationApi.updateUserLocation(user.getId(), longitude, latitude, address);
            return aBoolean;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }
}
