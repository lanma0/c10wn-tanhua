package com.tanhua.server.Service;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.common.enums.SexEnum;
import com.tanhua.common.pojo.HuanXinUser;
import com.tanhua.common.pojo.Question;
import com.tanhua.common.pojo.User;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.common.util.UserThreadLocal;
import com.tanhua.dubbo.server.api.*;
import com.tanhua.dubbo.server.enums.HuanXinMessageType;
import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.RecommendUser;
import com.tanhua.dubbo.server.pojo.UserLocation;
import com.tanhua.dubbo.server.pojo.Visitors;
import com.tanhua.dubbo.server.vo.UserLocationVo;
import com.tanhua.server.pojo.TodayBest;

import com.tanhua.server.vo.NearUserVo;
import com.tanhua.server.vo.PageResult;
import com.tanhua.server.vo.RecommendUserQueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.swing.*;
import java.util.*;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/12
 * @描述
 */
@Service
public class TodayBestService {
    @Autowired
    UserInfoService userInfoService;
    //    @Autowired
//    private UserService userService;
    @Autowired
    private RecommendUserService recommendUserService;
    @Value("${tanhua.commend.userId}")
    private Long defaultUser;

    @Value("${tanhua.sso.fateValue}")
    private Long defaultFateValue;

    @Value("${tanhua.default.recommend.users}")
    private String recommendUser;

    @Reference(version = "1.0.0")
    private UserLikeApi userLikeApi;

    @Autowired
    private IMService imService;

    @Reference(version = "1.0.0")
    private UserLocationApi userLocationApi;

    @Reference(version = "1.0.0")
    private HuanXinApi huanXinApi;


    @Autowired
    private QuestionService questionService;

    @Reference(version = "1.0.0")
    private VisitorsApi visitorsApi;


    public TodayBest earnTodayBest() {

        //1.校验token;
        User user = UserThreadLocal.get();
        //2.调用远程接口的查询功能；
        TodayBest todayBest = recommendUserService.earnRecommendUser(user.getId());
        if (null == todayBest) {
            //
            todayBest = new TodayBest();
            todayBest.setId(defaultUser);
            todayBest.setFateValue(defaultFateValue); //固定值

        }

        //查询数据库，将信息加载到todayBest中；
        UserInfo userInfo = userInfoService.getUserInfo(todayBest.getId());
        if (null == userInfo) {
            return todayBest;
        }

        todayBest.setAge(userInfo.getAge());
        todayBest.setAvatar(userInfo.getLogo());
        todayBest.setGender(userInfo.getSex() == 1 ? "man" : "women");
        todayBest.setNickname(userInfo.getNickName());
        todayBest.setTags(userInfo.getTags().split(","));
        //3.通过远程接口的数据查询数据库，并封装为对象。
        return todayBest;


    }

    public PageResult earnRecommendation(RecommendUserQueryParam queryParam) {
        //1.校验token;
        User user = UserThreadLocal.get();
        //2.调用远程接口的查询功能；

        PageInfo<RecommendUser> userPageInfo = recommendUserService.queryRecommendUserList(user.getId(), queryParam.getPage(), queryParam.getPagesize());

        PageResult pageResult = new PageResult();
        pageResult.setCounts(userPageInfo.getTotal());
        pageResult.setPage(userPageInfo.getPageNub());
        pageResult.setPagesize(userPageInfo.getPageSize());

        if (null == userPageInfo) {
            return pageResult;
        }
        List<RecommendUser> recommendUser = userPageInfo.getRecommendUser();
        Set<Long> userId = new HashSet<>();
        for (RecommendUser userInfo : recommendUser) {
            userId.add(userInfo.getUserId());
        }

        List<UserInfo> userInfos = userInfoService.getUserInfos(userId, queryParam);


        if (CollectionUtils.isEmpty(userInfos)) {
            return pageResult;
        }
        //将查询到用户基本信息，写入到todaybest集合中。用于返回
        List<TodayBest> todayBests = new ArrayList<>();
        for (UserInfo userInfo : userInfos) {
            TodayBest todayBest = new TodayBest();
            todayBest.setAge(userInfo.getAge());
            todayBest.setId(userInfo.getId());
            todayBest.setTags(userInfo.getTags().split(","));
            todayBest.setNickname(userInfo.getNickName());
            todayBest.setGender(userInfo.getSex() == 1 ? "men" : "women");
            todayBest.setAvatar(userInfo.getLogo());

            //写入缘分值返回值！！！；
            for (RecommendUser r : userPageInfo.getRecommendUser()) {
                if (r.getUserId().longValue() == userInfo.getId().longValue()) {
                    todayBest.setFateValue(Double.valueOf(Math.floor(r.getScore())).longValue());
                    break;
                }
            }
            todayBests.add(todayBest);
        }

        //由于推荐用户信息是轮询加进去的，所以我们需要对缘分值进行集合排序
//        Collections.sort(todayBests,((o1, o2) ->new Long( o1.getFateValue()-o2.getFateValue()).intValue()));
        Collections.sort(todayBests, new Comparator<TodayBest>() {
            @Override
            public int compare(TodayBest o1, TodayBest o2) {
                long l = o2.getFateValue() - o1.getFateValue();
                return new Long(l).intValue();
            }
        });
        pageResult.setItems(todayBests);
        return pageResult;
    }

    public TodayBest queryUserInfo(Long userId) {

        UserInfo userInfo = this.userInfoService.getUserInfo(userId);
        if (ObjectUtil.isEmpty(userInfo)) {
            return null;
        }

        TodayBest todayBest = new TodayBest();
        todayBest.setId(userId);
        todayBest.setAge(userInfo.getAge());
        todayBest.setGender(userInfo.getSex() == 1 ? "man" : "women");
        todayBest.setNickname(userInfo.getNickName());
        todayBest.setTags(Convert.toStrArray(StrUtil.split(userInfo.getTags(), ',')));
        todayBest.setAvatar(userInfo.getLogo());

        //缘分值
        User user = UserThreadLocal.get();
        todayBest.setFateValue(this.recommendUserService.queryScore(userId, user.getId()).longValue());
        this.visitorsApi.saveVisitor(userId, user.getId(), "个人主页");
        return todayBest;
    }

    public List<TodayBest> queryCardsList() {
        User user = UserThreadLocal.get();

        //查询推荐的userId 列表.
        Integer count = 50;
        List<RecommendUser> recommendUsers = recommendUserService.queryCardList(user.getId(), count);
        //1.第一种情况:返回的推荐用户是空值.
        if (CollUtil.isEmpty(recommendUsers)) {
            //那么,使用默认推荐的列表.
            List<String> split = StrUtil.split(recommendUser, ",");
            for (String userId : split) {
                RecommendUser recommendUser = new RecommendUser();

                recommendUser.setToUserId(user.getId());
                recommendUser.setUserId(Convert.toLong(userId));
                recommendUsers.add(recommendUser);
            }
        }

        //返回数据
        //1.判断集合是中的数据是否大于10;取小的那个.
        Set<RecommendUser> result = new HashSet<>();
        int showCount = Math.min(10, recommendUsers.size());
        //随机10个值
        for (int i = 0; i < showCount; i++) {
            //TODO 可能重复
            int index = RandomUtil.randomInt(0, recommendUsers.size());
            RecommendUser recommendUser = recommendUsers.get(index);
            result.add(recommendUser);
        }
        List<Object> userIdList = CollUtil.getFieldValues(result, "userId");
        List<UserInfo> userInfoList = this.userInfoService.getUserInfos(userIdList);

        //将结果分装到todayBests.
        List<TodayBest> todayBests = new ArrayList<>();
        for (UserInfo userInfo : userInfoList) {
            TodayBest todayBest = new TodayBest();
            todayBest.setId(userInfo.getUserId());
            todayBest.setAge(userInfo.getAge());
            todayBest.setAvatar(userInfo.getLogo());
            todayBest.setGender(userInfo.getSex() == 1 ? "men" : "women");
            todayBest.setNickname(userInfo.getNickName());
            todayBest.setTags(Convert.toStrArray(StrUtil.split(userInfo.getTags(), ',')));
            todayBest.setFateValue(0L);

            todayBests.add(todayBest);
        }
        return todayBests;


    }

    public Boolean likeUser(Long likeUserId) {
        User user = UserThreadLocal.get();
        //点赞.如果成功那么查询对象是否为自己好友.如果是,那么添加为好友.
        Boolean aBoolean = userLikeApi.likeUser(user.getId(), likeUserId);
        if (!aBoolean) {
            return false;
        }
        if (userLikeApi.isMutualLike(user.getId(), likeUserId)) {
            imService.contactUser(likeUserId);
        }
        return true;
    }

    public Boolean notLikeUser(Long likeUserId) {
        User user = UserThreadLocal.get();
        return this.userLikeApi.notLikeUser(user.getId(), likeUserId);
    }

    public List<NearUserVo> queryNearUser(String gender, String distance) {
        User user = UserThreadLocal.get();
        List<NearUserVo> nearUserVos = new ArrayList<>();
        //先查询自己的经纬度.
        UserLocationVo userLocationVo = userLocationApi.queryByUserId(user.getId());
        if (ObjectUtil.isEmpty(userLocationVo)) {
            return ListUtil.empty();
        }

        //查询自己附近的朋友
        PageInfo<UserLocationVo> pageInfo = this.userLocationApi.queryUserFromLocation(userLocationVo.getLongitude(),
                userLocationVo.getLatitude(),
                Convert.toDouble(distance),
                1,
                50
        );

        List<UserLocationVo> records = pageInfo.getRecommendUser();
        if (CollUtil.isEmpty(records)) {
            return ListUtil.empty();
        }

        List<Object> userId = CollUtil.getFieldValues(records, "userId");
        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper();
        queryWrapper.in("user_id", userId);
        if (StrUtil.isNotEmpty(gender)) {
            queryWrapper.eq("sex", StrUtil.equalsIgnoreCase(gender, "man") ? 1 : 0);
        }

        List<UserInfo> userInfos = userInfoService.getUserInfos(queryWrapper);

        for (UserInfo userInfo : userInfos) {
            NearUserVo nearUserVo = new NearUserVo();
            nearUserVo.setAvatar(userInfo.getLogo());
            nearUserVo.setNickname(userInfo.getNickName());
            nearUserVo.setUserId(userInfo.getUserId());
            nearUserVos.add(nearUserVo);
        }

        return nearUserVos;
    }

    //查询陌生人的信息；
    public String queryQuestion(Long userId) {
        Question question = this.questionService.queryQuestion(userId);
        if (ObjectUtil.isNotEmpty(question)) {
            return question.getTxt();
        }
        //默认的问题
        return "喜欢18还是180？";

    }

    public Boolean replyQuestion(Long userId, String reply) {
        User user = UserThreadLocal.get();


        //1.因为要让对方看到发送人的信息，所以要构建一个用户信息的消息体；
        UserInfo userInfo = userInfoService.getUserInfo(user.getId());

        //构建消息体；
        Map<String, Object> msg = new HashMap<>();
        //1.发送的Id
        msg.put("userId", user.getId());
        //2.发送消息的环信ip
        msg.put("huanXinId", "HX_" + user.getId());
        //3.送人的昵称。
        msg.put("nickname", userInfo.getNickName());
        //4.接受人的信息；
        msg.put("strangerQuestion", this.queryQuestion(userId));
        //.回复的内容
        msg.put("reply", reply);

        return huanXinApi.sendMsgFromAdmin("HX_" + user.getId(), HuanXinMessageType.TXT, JSONUtil.toJsonStr(msg));
    }
}