package com.tanhua.server.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.common.mapper.UserInfoMapper;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.server.vo.RecommendUserQueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/14
 * @描述
 */

@Service
public class UserInfoService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    public UserInfo getUserInfo(Long userId){
        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",userId);
        return userInfoMapper.selectOne(queryWrapper);
    }
    public List<UserInfo> getUserInfos(Set<Long> userId, RecommendUserQueryParam queryParam){
        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper();
        //通过id查找到推荐好友
        queryWrapper.in("user_id",userId);
//        if (queryParam.getAgeMin()!=null && queryParam.getAgeMax()!=null) {
//            //通过前端的 年龄 范围查找条件
//            queryWrapper.between("age",queryParam.getAgeMin(),queryParam.getAgeMax());
//        }
//        //通过前端的 城市 范围查找条件
//        if (queryParam.getCity()!=null){
//            queryWrapper.eq("city",queryParam.getCity());
//        }
//        //通过前端的 学历 范围查找条件
//        if (queryParam.getEducation()!=null){
//            queryWrapper.eq("edu",queryParam.getEducation());
//        }
//        //通过前端的 性别 范围查找条件
//        if (queryParam.getGender()!=null){
//            queryWrapper.eq("sex",queryParam.getGender()=="men"?1:2);
//        }
        List<UserInfo> userInfos = userInfoMapper.selectList(queryWrapper);
        return userInfos;
    }
    public List<UserInfo> getUserInfos(Collection<?> userId){
        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper();
        //通过id查找到推荐好友
        queryWrapper.in("user_id",userId);
        List<UserInfo> userInfos = userInfoMapper.selectList(queryWrapper);
        return userInfos;
    }
    public List<UserInfo> getUserInfos(QueryWrapper queryWrapper){
        //通过id查找到推荐好友
        List<UserInfo> userInfos = userInfoMapper.selectList(queryWrapper);
        return userInfos;
    }


    public Boolean updateUserInfoByUserId(UserInfo userInfo) {
        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userInfo.getUserId());
        return this.userInfoMapper.update(userInfo, queryWrapper) > 0;
    }
}
