package com.tanhua.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.common.pojo.HuanXinUser;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;


/**
 * @创建人 1anma0
 * @创建时间 2021/7/21
 * @描述
 */
@Component
public interface HuanXinMapper extends BaseMapper<HuanXinUser> {
}
