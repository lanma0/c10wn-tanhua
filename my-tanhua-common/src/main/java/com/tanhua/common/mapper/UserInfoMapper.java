package com.tanhua.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.common.pojo.UserInfo;
import org.springframework.stereotype.Repository;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/12
 * @描述
 */

@Repository
public interface UserInfoMapper extends BaseMapper<UserInfo> {
}
