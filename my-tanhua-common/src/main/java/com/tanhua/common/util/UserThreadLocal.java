package com.tanhua.common.util;

import com.tanhua.common.pojo.User;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/15
 * @描述
 */
public class UserThreadLocal {

    private static final ThreadLocal<User>  USER_THREAD_LOCAL=new ThreadLocal<>();

    /**
     * 将对象放入到ThreadLocal
     *
     * @param user
     */
    public static void set(User user){
        USER_THREAD_LOCAL.set(user);
    }


    /**
     * 返回当前线程中的User对象
     *
     * @return
     */
    public static User get(){
        return USER_THREAD_LOCAL.get();
    }



    /**
     * 删除当前线程中的User对象
     */
    public static void remove(){
        USER_THREAD_LOCAL.remove();
    }

}
