package com.tanhua.common.vo;

import lombok.Builder;
import lombok.Data;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/10
 * @描述
 */


@Data
@Builder
public class BuddhaLoadResoult {


    //唯一标号
    private String bId;
    //文件名
    private String bName;
    // 状态有：uploading done error removed
    private String bStatus;
    // 服务端响应内容，如：'{"status": "success"}'
    private String bResponse;
}
