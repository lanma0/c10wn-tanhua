package com.tanhua.common.service;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.PutObjectRequest;
import com.tanhua.common.config.AliyunConfig;
import com.tanhua.common.vo.BuddhaLoadResoult;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.UUID;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/10
 * @描述 头像上传到阿里云service
 */

@Service
public class BuddhaLoadService {


    @Autowired
    private OSSClient ossClient;

    @Autowired
    private AliyunConfig aliyunConfig;


    //定义可接受的后缀名
    private final static String IMAGE_SUFFIX[] = new String[]{".bmp", ".jpg", ".jpeg", ".gif", ".png"};

    public BuddhaLoadResoult buddhaLoad(MultipartFile picFile) {  //MultipartFile文件上传后的 字符集合
        //校验图片
        //定义图片是否合格
        boolean isPic=false;
        for (String suffix : IMAGE_SUFFIX) {
            if (com.baomidou.mybatisplus.core.toolkit.StringUtils.endsWith(picFile.getOriginalFilename(),suffix)){ //picFile.getOriginalFilename():拿到文件原始名字
                //如果成功说明,符合图片文件规则.
                isPic=true;
                break;
            }

        }
        //如果都不符合那么说明,文件不合规,的返回失败.error
        if (!isPic){
            return BuddhaLoadResoult.builder().bStatus("error").build();
        }
        //如果到这,说明文件合规,上传阿里云
        String filename = picFile.getOriginalFilename();
        String filePath = getFilePath(filename);
        try {
            PutObjectRequest putObjectRequest = new PutObjectRequest(aliyunConfig.getBucketName(),filePath,new ByteArrayInputStream(picFile.getBytes()));
            ossClient.putObject(putObjectRequest);
        } catch (IOException e) {
            //异常,上传失败.
            e.printStackTrace();
            return BuddhaLoadResoult.builder().bStatus("error").build();
        }
        //上传成功.

        return BuddhaLoadResoult.builder().bStatus("done").bName(aliyunConfig.getUrlPrefix()+filePath).bId(UUID.randomUUID().toString()).build();

    }
    public String getFilePath(String fileName){
        Calendar rightnow = Calendar.getInstance();
        String aliyunFileName = System.currentTimeMillis()+ RandomUtils.nextInt(100,999)+"."+StringUtils.substringAfterLast(fileName,".");
        String path = ("tanhua-sso/"+rightnow.get(Calendar.YEAR)+"/"+(rightnow.get(Calendar.MONTH)+1)+"/"+rightnow.get(Calendar.DAY_OF_MONTH)+"/"+aliyunFileName);
        return path;


    }
}
