package com.tanhua.common.config;

import com.aliyun.oss.OSSClient;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/9
 * @描述
 */

@Configuration
@PropertySource("classpath:aliyun.properties")
@ConfigurationProperties(prefix = "aliyun")
@Data
public class AliyunConfig {

    private  String endpoint;
    private String accessKeyId;
    private String  accessKeySecret;
    private  String bucketName;
    private String urlPrefix;

    @Bean
    public OSSClient ossClient(){
        OSSClient ossClient = new OSSClient(endpoint,accessKeyId,accessKeySecret);
        return ossClient;
    }
}
