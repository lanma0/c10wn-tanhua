package com.tanhua.common.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User extends BasePojo {
    private Long id;
    private String account;
    @JsonIgnore
    //json序列化的手忽略？  为什么要忽悠
    private String password;
}
