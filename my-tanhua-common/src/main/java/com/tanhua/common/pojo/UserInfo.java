package com.tanhua.common.pojo;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo extends BasePojo {
    private Long id;//
    private Long userId;//  '用户id',
    private String nickName;//  '昵称',
    private String logo;//  '用户头像',
    private String tags;//  '用户标签：多个用逗号分隔',
    private int sex;//  '性别，1-男，2-女，3-未知',
    private int age;//  '用户年龄',
    private String edu;//   '学历',
    private String city;//   '居住城市',
    private String birthday;//   '生日',
    private String coverPic;//   '封面图片',
    private String industry;//   '行业',
    private String income;//    '收入',
    private String marriage;//   '婚姻状态',
    @TableField(fill = FieldFill.INSERT)
    private Date created;  //创建时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updated; //更新时间

}
