package com.tanhua.dubbo.server.service;

import com.tanhua.common.pojo.HuanXinUser;
import com.tanhua.dubbo.server.api.HuanXinApi;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/21
 * @描述
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class HuanXinApiImplTest {
    @Autowired
    private HuanXinApi huanXinApi;

    @Test
    public void getToken() {
    }

    @Test
    public void testRegisterAllUser(){
        for (int i = 1; i < 100; i++) {
            this.huanXinApi.register(Long.valueOf(i));
        }
    }

    @Test
    public void queryHuanXinUser() {
        HuanXinUser huanXinUser = huanXinApi.queryHuanXinUser(1L);
        System.out.println(huanXinUser);
    }

    @Test
    public void queryUserByUserName() {
        HuanXinUser hx_1 = huanXinApi.queryUserByUserName("HX_1");
        System.out.println(hx_1);
    }

    @Test
    public void addUserFriend() {

    }

    @Test
    public void removeUserFriend() {
    }
}