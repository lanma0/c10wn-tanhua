package com.tanhua.dubbo.server.api;

import cn.hutool.core.util.IdUtil;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.Method;
import cn.hutool.json.JSONException;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.common.mapper.HuanXinMapper;
import com.tanhua.common.pojo.HuanXinUser;
import com.tanhua.dubbo.server.config.HuanXinConfig;

import com.tanhua.dubbo.server.enums.HuanXinMessageType;
import com.tanhua.dubbo.server.service.RequestService;
import com.tanhua.dubbo.server.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/20
 * @描述
 */

@Service(version = "1.0.0")
@Component
@Slf4j
public class HuanXinApiImpl implements HuanXinApi {

    @Autowired
    RequestService requestService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private HuanXinConfig huanXinConfig;
    @Autowired
    private HuanXinMapper huanXinMapper;

    @Override
    public String getToken() {
        String token = tokenService.getToken();
        return token;
    }


    @Override
    public Boolean register(Long userId) {
        //批量注册
        //1.构造设置url.
        String targetUrl = this.huanXinConfig.getUrl()
                + this.huanXinConfig.getOrgName() + "/" +
                this.huanXinConfig.getAppName() + "/users";


        //2.构造参数
        HuanXinUser huanXinUser = new HuanXinUser();
        huanXinUser.setUsername("HX_" + userId);  // 用户名
        huanXinUser.setPassword(IdUtil.simpleUUID()); //随机生成的密码
        //3.调用请求方法.
        HttpResponse execute = requestService.execute(targetUrl, JSONUtil.toJsonStr(huanXinUser), Method.POST);

        //4.判断是否成功.
        if (execute.isOk()) {
            //5.如果成功将放回值保存到数据库中.
            huanXinUser.setUserId(userId);
            huanXinUser.setCreated(new Date());
            huanXinUser.setUpdated(huanXinUser.getCreated());
            this.huanXinMapper.insert(huanXinUser);
        }
        return false;
    }

    @Override
    public HuanXinUser queryHuanXinUser(Long userId) {
        //1.构建查询条件
        QueryWrapper<HuanXinUser> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        //2.查询条件,返回.
        return this.huanXinMapper.selectOne(wrapper);
    }

    @Override
    public HuanXinUser queryUserByUserName(String userName) {
        //构造条件
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("username", userName);
        //查询数据库
        HuanXinUser huanXinUser = huanXinMapper.selectOne(queryWrapper);

        return huanXinUser;
    }

    //添加好友和删除好友的参数一样,都是通过一个url.但是不同的访问方式.
    @Override

    public Boolean addUserFriend(Long userId, Long friendId) {
        //0.通过id查找环信的账号信息(这里不需要,因为i,固定是HX_id的模式注册的.)

        //设置url
        String targetUrl = this.huanXinConfig.getUrl()
                + this.huanXinConfig.getOrgName() + "/"
                + this.huanXinConfig.getAppName() + "/users/HX_" +
                userId + "/contacts/users/HX_" + friendId;

        //2.发起注册.
        try {
            return this.requestService.execute(targetUrl, null, Method.POST).isOk();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }

    @Override
    public Boolean removeUserFriend(Long userId, Long friendId) {
        String targetUrl = this.huanXinConfig.getUrl()
                + this.huanXinConfig.getOrgName() + "/"
                + this.huanXinConfig.getAppName() + "/users/HX_" +
                userId + "/contacts/users/HX_" + friendId;

        try {
            // 404 -> 对方未在环信注册
            return this.requestService.execute(targetUrl, null, Method.DELETE).isOk();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 添加失败
        return false;
    }

    @Override
    public Boolean sendMsgFromAdmin(String targetUserName, HuanXinMessageType huanXinMessageType, String msg) {

        //1.通过targetuserName 拿到对象的环信信息；

        //2.

        String targetUrl = this.huanXinConfig.getUrl()
                + this.huanXinConfig.getOrgName() + "/"
                + this.huanXinConfig.getAppName() + "/messages";

        try {
            String param = JSONUtil.createObj().set("target_type", "users")
                    .set("target", JSONUtil.createArray().set(targetUserName))
                    .set("msg", msg)
                    .set("type", huanXinMessageType.getType()).toString();
//                .set("from",)

            HttpResponse httpResponse = requestService.execute(targetUrl, param, Method.POST);


            return httpResponse.isOk();
        } catch (JSONException e) {
            log.error("发送消息失败~ targetUserName = " + targetUserName+", type = " + huanXinMessageType.getType()+", msg = " + msg, e);
        }
        return false;
    }
}
