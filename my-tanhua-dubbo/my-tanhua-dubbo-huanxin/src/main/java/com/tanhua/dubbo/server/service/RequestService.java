package com.tanhua.dubbo.server.service;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.Method;
import com.tanhua.dubbo.server.exception.UnauthorizedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/20
 * @描述      用来访问环信接口的服务
 */

@Service
@Slf4j
public class RequestService {

    @Autowired
    TokenService tokenService;

    @Retryable(value = UnauthorizedException.class, maxAttempts = 5, backoff = @Backoff(delay = 2000L, multiplier = 2))
    public HttpResponse execute(String url, String body, Method mthod){
        String token = this.tokenService.getToken();
        HttpRequest httpRequest;

        //判断是什么请求,使用不同的请求进行访问.
        switch (mthod) {
            case POST: {
                httpRequest = HttpRequest.post(url);
                break;
            }
            case DELETE: {
                httpRequest = HttpRequest.delete(url);
                break;
            }
            case PUT: {
                httpRequest = HttpRequest.put(url);
                break;
            }
            case GET: {
                httpRequest = HttpRequest.get(url);
                break;
            }
            default: {
                return null;
            }
        }
        HttpResponse httpResponse = httpRequest.header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .body(body)
                .timeout(2000)
                .execute();

        if (httpResponse.getStatus()==401) {
            //重新获得token,防止是token过期等问题.
            tokenService.getHxToken();
            throw new  UnauthorizedException(url,body,mthod);
        }
        return httpResponse;

    }
}
