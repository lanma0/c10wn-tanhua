package com.tanhua.dubbo.server.service;


import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.tanhua.dubbo.server.config.HuanXinConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/20
 * @描述 用来生成
 */
@Service
public class TokenService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    private final static String HX_TOKEN_KEY = "HX_TOKEN";

    @Autowired
    private HuanXinConfig huanXinConfig;


    public String getToken() {
        //先从redis中获取token
        String s = redisTemplate.opsForValue().get(HX_TOKEN_KEY);
        if (StringUtils.isNotEmpty(s)) {
            return s;
        }
        //如果不为空,那么就需要去访问环信获得token
        String hxToken = getHxToken();
        return hxToken;
    }


    public String getHxToken() {
        //定义访问地址
        String targetUrl = this.huanXinConfig.getUrl() +
                this.huanXinConfig.getOrgName() + "/" +
                this.huanXinConfig.getAppName() + "/token";


        //配置访问参数
        Map<String, Object> param = new HashMap<>();
        param.put("grant_type", "client_credentials");
        param.put("client_id", this.huanXinConfig.getClientId());
        param.put("client_secret", this.huanXinConfig.getClientSecret());

        //进行访问得到返回值,进行判断.

        HttpResponse response = HttpRequest.post(targetUrl).body(JSONUtil.toJsonStr(param)).timeout(20000).execute();
        if (!response.isOk()) {
            //获得token失败.
            return null;
        }
        //获得内容
        String body = response.body();
        JSONObject jsonObject = JSONUtil.parseObj(body);
        String access_token = jsonObject.getStr("access_token");
        if (StringUtils.isNotEmpty(access_token)) {
            //设置缓冲时间,防止,token过期情况,将时间减少30分钟.
            long timeout = jsonObject.getLong("expires_in") - 60 * 30;
            //将token保存到redis中,方便下次取
            redisTemplate.opsForValue().set(HX_TOKEN_KEY, access_token, timeout, TimeUnit.SECONDS);
            return access_token;

        }
        return null;
    }
}