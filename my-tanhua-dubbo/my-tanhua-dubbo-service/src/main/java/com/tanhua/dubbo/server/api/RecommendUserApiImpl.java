package com.tanhua.dubbo.server.api;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.RecommendUser;
import com.tanhua.dubbo.server.pojo.UserLike;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;


/**
 * @创建人 1anma0
 * @创建时间 2021/7/12
 * @描述
 */


@Service(version = "1.0.0") //申明这是一个dubbo服务
//@org.springframework.stereotype.Service
public class RecommendUserApiImpl implements RecommendUserApi {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private  UserLikeApi userLikeApi;

    @Override
    public RecommendUser getMaxScoreUser(Long userId) {
        //1.编写mongdb的查询条件
        /**
         * 1 创建一个query对象（用来封装所有条件对象)，再创建一个criteria对象（用来构建条件）
         * 2 精准条件：criteria.and(“key”).is(“条件”)
         * 模糊条件：criteria.and(“key”).regex(“条件”)
         * 3、封装条件：query.addCriteria(criteria)
         * 4、大于（创建新的criteria）：Criteria gt = Criteria.where(“key”).gt（“条件”）
         * 小于（创建新的criteria）：Criteria lt = Criteria.where(“key”).lt（“条件”）
         * 5、Query.addCriteria(new Criteria().andOperator(gt,lt));
         * 6、一个query中只能有一个andOperator()。其参数也可以是Criteria数组。
         * 7、排序 ：query.with（new Sort(Sort.Direction.ASC, "age"). and(new Sort(Sort.Direction.DESC, "date")))
         */
        Query toUserId = Query.query(Criteria.where("toUserId").is(userId)).with(Sort.by(Sort.Order.desc("score"))).limit(1);

        //2.查询到最高的缘分;
        return mongoTemplate.findOne(toUserId,RecommendUser.class);
    }

    @Override
    public PageInfo<RecommendUser> getUserPageInfo(Long userId, Integer pageNum, Integer pageSize) {
        //实现了pageable接口。
        PageRequest score = PageRequest.of(pageNum-1, pageSize, Sort.by(Sort.Order.desc("score")));
        Query toUserId = Query.query(Criteria.where("toUserId").is(userId)).with(score);


        List<RecommendUser> pageInfos = mongoTemplate.find(toUserId, RecommendUser.class);
        return new PageInfo<>(0,pageNum,pageSize,pageInfos);
    }

    @Override
    //查询缘分值。就也是一个简单的mondb查询。
    public Double queryScore(Long userId, Long toUserId) {

            Query query = Query.query(Criteria.where("toUserId").is(toUserId)
                    .and("userId").is(userId));
            RecommendUser recommendUser = this.mongoTemplate.findOne(query, RecommendUser.class);
            if (null != recommendUser) {
                return recommendUser.getScore();
            }
            return null;
    }

    @Override
    public List<RecommendUser> queryCardList(Long userId, Integer count) {

        //1.将结果排除 已喜欢的里列表.
        List<Long> likeList = userLikeApi.queryLikeList(userId);


        //2.将推荐的用户排除 不喜欢的
        List<Long> notLikeList = userLikeApi.queryNotLikeList(userId);

        ArrayList<Long> excludeUserIdList = new ArrayList<>();
        excludeUserIdList.addAll(likeList);
        excludeUserIdList.addAll(notLikeList);

        //3.构造查询条件
        PageRequest pageRequest =PageRequest.of(0,count,Sort.by(Sort.Order.desc("score")));
        Criteria criteria = Criteria.where("toUserId").in(userId);
        if (CollUtil.isNotEmpty(excludeUserIdList)){
            criteria.andOperator(Criteria.where("userId").nin(excludeUserIdList));
        }
        Query query = Query.query(criteria).with(pageRequest);
        List<RecommendUser> recommendUsers = mongoTemplate.find(query, RecommendUser.class);

        return recommendUsers;
    }
}
