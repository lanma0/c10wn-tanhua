package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.enums.IdType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/15
 * @描述
 */
@Service
public class IdService {


    @Autowired
    private RedisTemplate<String ,String> redisTemplate;

    /**
     *@描述 getAutoId:使用redis的String 实现自增长的方法.
     *@参数 []
     *@返回值 java.lang.Long
     *@创建人  1anma0
     *@创建时间 2021/7/15
     *@修改人和其它信息
     */

    public Long  getAutoId(IdType idType){
        String idKey = "TANHUA_ID_" + idType.toString();

        Long increment = redisTemplate.opsForValue().increment(idKey);
        return increment;
    }

}
