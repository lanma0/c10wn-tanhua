package com.tanhua.dubbo.server.api;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.RecommendUser;
import com.tanhua.dubbo.server.pojo.Visitors;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/24
 * @描述
 */
public class VisitorsApiImpl implements VisitorsApi {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private RedisTemplate redisTemplate;


    private static final String VISITOR_REDIS_KEY = "VISITOR_USER";

    @Override
    public String saveVisitor(Long userId, Long visitorUserId, String from) {
        //校验 传输的参数是否未空
        if (!ObjectUtil.isAllNotEmpty(userId, visitorUserId, from)) {
            return null;
        }

        //mongoTemplate.upsert(query, update, Visitors.class);直接用这个方法可以完成，没有就新增，有就修改。
        //校验今天是否已经访问。
        String today = DateUtil.today();
        Long minDate = DateUtil.parseDateTime(today + " 00:00:00").getTime();
        Long maxDate = DateUtil.parseDateTime(today + " 23:59:59").getTime();
        Query query = Query.query(Criteria.where("userId").is(userId)
                .and("visitorUserId").is(visitorUserId)
                .andOperator(Criteria.where("date").gte(minDate), Criteria.where("date").lte(maxDate)));

        long count = this.mongoTemplate.count(query, Visitors.class);
        if (count > 0) {
            //说明今天已经记录了。
            return null;
        }
        Visitors visitors = new Visitors();
        visitors.setFrom(from);
        visitors.setVisitorUserId(visitorUserId);
        visitors.setUserId(userId);
        visitors.setDate(System.currentTimeMillis());
        visitors.setId(ObjectId.get());
        //存储数据
        this.mongoTemplate.save(visitors);

        return visitors.getId().toHexString();
    }

    @Override
    public List<Visitors> queryMyVisitor(Long userId) {

        //1.先查询redis中上次访问的时间。
        Long date = Convert.toLong(this.redisTemplate.opsForHash().get(VISITOR_REDIS_KEY, String.valueOf(userId)));
        PageRequest pageRequest =PageRequest.of(0,5, Sort.by(Sort.Order.desc("date")));

        Query query = Query.query(Criteria.where("userId").in(userId)).with(pageRequest);


        if (ObjectUtil.isNotEmpty(date)){
            query.addCriteria(Criteria.where("date").gte(date));
        }
        List<Visitors> visitors = mongoTemplate.find(query, Visitors.class);


        //查询后，进行查询得分
        for (Visitors visitor : visitors) {
            Query queryScore = Query.query(Criteria.where("toUserId")
                    .is(userId).and("userId").is(visitor.getVisitorUserId())
            );
            RecommendUser recommendUser = this.mongoTemplate.findOne(queryScore, RecommendUser.class);
            if(ObjectUtil.isNotEmpty(recommendUser)){
                visitor.setScore(recommendUser.getScore());
            }else {
                //默认得分
                visitor.setScore(90d);
            }

        }
        return visitors;

    }

    @Override
    public PageInfo<Visitors> queryTopVisitor(Long userId, Integer page, Integer pageSize) {
        return null;
    }
}
