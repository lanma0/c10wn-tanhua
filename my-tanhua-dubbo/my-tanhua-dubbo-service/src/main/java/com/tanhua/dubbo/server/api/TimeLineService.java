package com.tanhua.dubbo.server.api;

import cn.hutool.core.collection.CollUtil;
import com.tanhua.dubbo.server.pojo.TimeLine;
import com.tanhua.dubbo.server.pojo.UserRelation;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/15
 * @描述
 */

@Service
@Slf4j
public class TimeLineService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Async
    public CompletableFuture<String> saveTimeLine(Long userId, ObjectId pulishId,Long date){
        //查询好友列表

        try {
            Query query = Query.query(Criteria.where("userId").is(userId));
            List<UserRelation> userRelations = mongoTemplate.find(query, UserRelation.class);

            List<Object> friendId = CollUtil.getFieldValues(userRelations, "friendId");

            for (Object friendid : friendId) {
                TimeLine timeLine = new TimeLine();
                timeLine.setDate(date);
                timeLine.setId(ObjectId.get());
                timeLine.setPublishId(pulishId);
                timeLine.setUserId(userId);
                mongoTemplate.save(timeLine,"quanzi_time_line_"+friendid);
            }
        } catch (Exception e) {
            log.error("写入好友时间线表失败~ userId = " + userId + ", publishId = " + pulishId, e);
            //TODO 事务回滚问题
            return CompletableFuture.completedFuture("error");
        }
        return CompletableFuture.completedFuture("ok");


    }
}
