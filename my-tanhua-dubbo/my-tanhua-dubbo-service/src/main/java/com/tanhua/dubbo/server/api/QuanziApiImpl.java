package com.tanhua.dubbo.server.api;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.PageUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.mongodb.client.result.DeleteResult;
import com.tanhua.dubbo.server.enums.CommentType;
import com.tanhua.dubbo.server.enums.IdType;
import com.tanhua.dubbo.server.pojo.*;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import sun.net.www.protocol.http.HttpURLConnection;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * @创建人 1anma0
 * @创建时间 2021/7/14
 * @描述
 */
@Service(version = "1.0.0") //申明这是一个dubbo服务
public class QuanziApiImpl implements QuanZiApi {
    //评论数据存储在Redis中key的前缀
    private static final String COMMENT_REDIS_KEY_PREFIX = "QUANZI_COMMENT_";
    //用户是否点赞的前缀
    private static final String COMMENT_USER_LIEK_REDIS_KEY_PREFIX = "USER_LIKE_";
    //用户是否喜欢的前缀
    private static final String COMMENT_USER_LOVE_REDIS_KEY_PREFIX = "USER_LOVE_";
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private IdService idService;


    @Autowired
    private TimeLineService timeLineService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private VideoApiImpl videoApi;


    @Override
    public PageInfo<Publish> queryPublishList(Integer page, Integer pagesize, Long userId) {
        PageInfo<Publish> pageInfo = new PageInfo<>();
        pageInfo.setPageNub(page);
        pageInfo.setPageSize(pagesize);
        //需求，找某个人的圈子。
        //1。通过相册表可以得到所有的好友的publishId和created
        PageRequest pageRequest = PageRequest.of(page - 1, pagesize, Sort.by(Sort.Order.desc("date")));

        Query query = new Query().with(pageRequest);
        List<PhotoAlbum> photoAlbums = mongoTemplate.find(query, PhotoAlbum.class, "quanzi_time_line_" + userId);

        if (CollectionUtils.isEmpty(photoAlbums)) {
            return pageInfo;
        }


        //2通过publishId 可以找到对应所有的Publish
        List<ObjectId> publishId = new ArrayList<>();
        for (PhotoAlbum photoAlbum : photoAlbums) {
            publishId.add(photoAlbum.getPublishId());
        }
        Query queryPulish = Query.query(Criteria.where("id").in(publishId)).with(Sort.by(Sort.Order.desc("created")));
        List<Publish> publishList = mongoTemplate.find(queryPulish, Publish.class);
        if (CollectionUtils.isEmpty(publishList)) {
            return null;
        }
        pageInfo.setRecommendUser(publishList);
        return pageInfo;
    }

    @Override
    public PageInfo<Publish> queryRecommendPublishList(Integer page, Integer pagesize, Long userId) {
        PageInfo<Publish> pageInfo = new PageInfo<>();
        pageInfo.setPageNub(page);
        pageInfo.setPageSize(pagesize);
        //1.构建rediskey
        String key = "QUANZI_PUBLISH_RECOMMEND_" + userId;
        //2.去redis中查询，数据，相当于拿到了要查询的id.接下来就和上面的一样了。
        Object value = redisTemplate.opsForValue().get(key);
        String monids = Convert.toStr(value);
        if (StringUtils.isEmpty(monids)) {
            return pageInfo;
        }
        List<String> split = StrUtil.split(monids, ",");


//这里是进行分页计算。。。。有点没看懂。就是计算分页的逻辑，需要待会回顾一下。
        int[] startEnd = PageUtil.transToStartEnd(page - 1, pagesize);
        int startIndex = startEnd[0]; //开始
        int endIndex = Math.min(startEnd[1], split.size()); //结束

        List<Long> longs = new ArrayList<Long>();
        for (int i = startIndex; i < endIndex; i++) {
            longs.add(Long.parseLong(split.get(i)));
        }

        Query queryPulish = Query.query(Criteria.where("pid").in(longs)).with(Sort.by(Sort.Order.desc("created")));
        List<Publish> publishList = mongoTemplate.find(queryPulish, Publish.class);
        if (CollectionUtils.isEmpty(publishList)) {
            return null;
        }
        pageInfo.setRecommendUser(publishList);
        return pageInfo;

    }

    @Override
    public String savePublish(Publish publish) {

        //1.判断pulish 里面的参数是否合规  userId 和contenx
        if (!ObjectUtil.isAllNotEmpty(publish.getUserId(), publish.getText())) {
            return null;

        }
        //1.1设置参数(主键id,创建时间.自增id():通过redis的原子性,单线程来控制自增id;
        publish.setId(ObjectId.get());

        try {
            publish.setPid(idService.getAutoId(IdType.PUBLISH));
            long date = System.currentTimeMillis();
            publish.setCreated(date);
            //2.将pulish 存到 mon Pubish
            mongoTemplate.save(publish);


            //3.将pulish 写入 mon 相册表
            PhotoAlbum photoAlbum = new PhotoAlbum();
            photoAlbum.setCreated(date);
            photoAlbum.setPublishId(publish.getId());
            photoAlbum.setId(ObjectId.get());

            mongoTemplate.save(photoAlbum, "quanzi_album_" + publish.getUserId());


            //4.将plulish 吸入 mon 时间线表(使用异步的方法写入.)
            timeLineService.saveTimeLine(publish.getUserId(), publish.getId(), date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        //返回pulish的id 值
        return publish.getId().toHexString();

    }

    //-----------------点赞相关逻辑---------------------------

    @Override
    public Publish queryPublishById(String id) {
        ObjectId objectId = new ObjectId(id);
        Publish publish = mongoTemplate.findById(objectId, Publish.class);
        return publish;
    }

    @Override
    /**
     *@描述 likeComment:通过将点赞数据修改到修改的操作表。
     *@参数 [UserId, publishId]
     *@返回值 boolean
     *@创建人 1anma0
     *@创建时间 2021/7/16
     *@修改人和其它信息
     */
    public boolean likeComment(Long UserId, String publishId) {
        //1.查询是否点赞。
        boolean b = UserIsLike(UserId, publishId);
        if (b) {
            return false;
        }
        //2.将数据存入到quanzi_comment
        boolean b1 = saveComment(UserId, publishId, CommentType.LIKE, null);
        if (!b1) {
            return false;
        }
        //3.将点赞数更新到缓存中。
        String likeKey = COMMENT_REDIS_KEY_PREFIX + publishId;
        String likeHkey = CommentType.LIKE.toString();
        redisTemplate.opsForHash().increment(likeKey, likeHkey, 1);

        //4.这里要注意，如果点赞了。还需要将点赞的状态进行修改。并保存到redis中。
        String likeUserHkey = COMMENT_USER_LIEK_REDIS_KEY_PREFIX + UserId;
        redisTemplate.opsForHash().put(likeKey, likeUserHkey, "1");

        return true;
    }

    @Override
    public boolean removeLikeComment(Long userId, String publishId) {
        //1.查询是否点赞。
        boolean b = UserIsLike(userId, publishId);
        if (!b) {
            return false;
        }


        //2.将数据从quanzi_comment中删除
        boolean b1 = removeComment(userId, publishId, CommentType.LIKE);
        if (!b1) {
            return false;
        }
        String likeKey = COMMENT_REDIS_KEY_PREFIX + publishId;
        String likeUserHkey = COMMENT_USER_LIEK_REDIS_KEY_PREFIX + userId;

        redisTemplate.opsForHash().delete(likeKey, likeUserHkey);
        //3.修改点赞的数量
        String likeHkey = CommentType.LIKE.toString();
        redisTemplate.opsForHash().increment(likeKey, likeHkey, -1);

        return true;
    }

    @Override
    public Long getLikeCount(String publishId) {
        String likeKey = COMMENT_REDIS_KEY_PREFIX + publishId;
        String likeHkey = CommentType.LIKE.toString();
        Object o = redisTemplate.opsForHash().get(likeKey, likeHkey);
        if (ObjectUtil.isNotEmpty(o)) {
            return Convert.toLong(o);
        }
        long count = getCommentCount(publishId, CommentType.LIKE);
        //将结果保存到redis中。
        redisTemplate.opsForHash().put(likeKey, likeHkey, String.valueOf(count));
        return count;
    }

    private long getCommentCount(String publishId, CommentType commentType) {
        //从mondb中查询数量。
        Query query = Query.query(Criteria.where("publishId").is(new ObjectId(publishId)).and("commentType").is(commentType.getType()));
        return mongoTemplate.count(query, Comment.class);
    }

    @Override
    public boolean UserIsLike(Long userId, String publishId) {
        String likeKey = COMMENT_REDIS_KEY_PREFIX + publishId;
        String likeUserHkey = COMMENT_USER_LIEK_REDIS_KEY_PREFIX + userId;

        //尝试去redis 中去找状态。
        Object o = redisTemplate.opsForHash().get(likeKey, likeUserHkey);
        if (ObjectUtil.isNotEmpty(o)) {
            return StringUtils.equals(Convert.toStr(o), "1");
        }
        //再去mondb中找

        Query query = Query.query(Criteria.where("publishId").is(new ObjectId(publishId))
                .and("userId").is(userId)
                .and("CommentType").is(CommentType.LIKE.getType()));
        long count = mongoTemplate.count(query, Comment.class);
        if (count == 0) {
            return false;
        }
        //将结果存到redis中
        redisTemplate.opsForHash().put(likeKey, likeUserHkey, "1");

        return true;
    }

    @Override
    public boolean loveComment(Long UserId, String publishId) {
        //1.查询是否点赞。
        boolean b = UserIsLove(UserId, publishId);
        if (b) {
            return false;
        }
        //2.将数据存入到quanzi_comment
        boolean b1 = saveComment(UserId, publishId, CommentType.LOVE, null);
        if (!b1) {
            return false;
        }
        //3.将点赞数更新到缓存中。
        String likeKey = COMMENT_REDIS_KEY_PREFIX + publishId;
        String likeHkey = CommentType.LOVE.toString();
        redisTemplate.opsForHash().increment(likeKey, likeHkey, 1);

        //4.这里要注意，如果点赞了。还需要将点赞的状态进行修改。并保存到redis中。
        String likeUserHkey = COMMENT_USER_LOVE_REDIS_KEY_PREFIX + UserId;
        redisTemplate.opsForHash().put(likeKey, likeUserHkey, "1");

        return true;
    }

    @Override
    public boolean removeLoveComment(Long userId, String publishId) {
        //1.查询是否点赞。
        boolean b = UserIsLove(userId, publishId);
        if (!b) {
            return false;
        }


        //2.将数据从quanzi_comment中删除
        boolean b1 = removeComment(userId, publishId, CommentType.LOVE);
        if (!b1) {
            return false;
        }
        String likeKey = COMMENT_REDIS_KEY_PREFIX + publishId;
        String likeUserHkey = COMMENT_USER_LOVE_REDIS_KEY_PREFIX + userId;

        redisTemplate.opsForHash().delete(likeKey, likeUserHkey);
        //3.修改点赞的数量
        String likeHkey = CommentType.LOVE.toString();
        redisTemplate.opsForHash().increment(likeKey, likeHkey, -1);

        return true;
    }

    @Override
    public Long getLoveCount(String publishId) {
        String likeKey = COMMENT_REDIS_KEY_PREFIX + publishId;
        String likeHkey = CommentType.LOVE.toString();
        Object o = redisTemplate.opsForHash().get(likeKey, likeHkey);
        if (ObjectUtil.isNotEmpty(o)) {
            return Convert.toLong(o);
        }
        long count = getCommentCount(publishId, CommentType.LOVE);
        //将结果保存到redis中。
        redisTemplate.opsForHash().put(likeKey, likeHkey, String.valueOf(count));
        return count;
    }


    @Override
    public boolean UserIsLove(Long userId, String publishId) {
        String likeKey = COMMENT_REDIS_KEY_PREFIX + publishId;
        String likeUserHkey = COMMENT_USER_LOVE_REDIS_KEY_PREFIX + userId;

        //尝试去redis 中去找状态。
        Object o = redisTemplate.opsForHash().get(likeKey, likeUserHkey);
        if (ObjectUtil.isNotEmpty(o)) {
            return StringUtils.equals(Convert.toStr(o), "1");
        }
        //再去mondb中找

        Query query = Query.query(Criteria.where("publishId").is(new ObjectId(publishId))
                .and("userId").is(userId)
                .and("CommentType").is(CommentType.LOVE.getType()));
        long count = mongoTemplate.count(query, Comment.class);
        if (count == 0) {
            return false;
        }
        //将结果存到redis中
        redisTemplate.opsForHash().put(likeKey, likeUserHkey, "1");
        return true;
    }

    @Override
    public PageInfo<Comment> queryCommentList(String publishId, Integer page, Integer pageSize) {
        //通过query 查找到comment的表中。
        PageInfo<Comment> commentPageInfo = new PageInfo<>();
        commentPageInfo.setPageNub(page);
        commentPageInfo.setPageSize(pageSize);
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Order.desc("created")));

        Query query = Query.query(Criteria.where("publishId").is(new ObjectId(publishId)).and("commentType").is(CommentType.COMMENT.getType()))
                .with(pageRequest);
        List<Comment> comments = mongoTemplate.find(query, Comment.class);
        commentPageInfo.setRecommendUser(comments);
        return commentPageInfo;
    }

    @Override
    public Boolean saveComment(String publishId, Long userId, String content) {
//        Comment comment = new Comment();
//        comment.setId(new ObjectId());
//        comment.setPublishUserId(userId);
//        comment.setPublishId(new ObjectId(publishId));
//        comment.setCreated(System.currentTimeMillis());
//        comment.setContent(content);
//        comment.setCommentType(CommentType.COMMENT.getType());
//        comment.setIsParent(false);
//        Comment result = mongoTemplate.save(comment);
//        if (result == null) {
//            return false;
//        }
//        return true;

        return saveComment(userId, publishId, CommentType.COMMENT, content);
    }

    @Override
    public Long getCommentCount(String publishId) {
        String likeKey = COMMENT_REDIS_KEY_PREFIX + publishId;
        String likeHkey = CommentType.COMMENT.toString();
        Object o = redisTemplate.opsForHash().get(likeKey, likeHkey);
        if (ObjectUtil.isNotEmpty(o)) {
            return Convert.toLong(o);
        }
        long count = getCommentCount(publishId, CommentType.COMMENT);
        //将结果保存到redis中。
        redisTemplate.opsForHash().put(likeKey, likeHkey, String.valueOf(count));
        return count;
    }

    @Override
    public PageInfo<Comment> queryLikeCommentListByUser(Long userId, Integer page, Integer pageSize) {

        return this.queryCommentListByUser(userId, CommentType.LIKE, page, pageSize);
    }

    @Override
    public PageInfo<Comment> queryLoveCommentListByUser(Long userId, Integer page, Integer pageSize) {
        return this.queryCommentListByUser(userId, CommentType.LOVE, page, pageSize);

    }

    @Override
    public PageInfo<Comment> queryCommentListByUser(Long userId, Integer page, Integer pageSize) {
        return this.queryCommentListByUser(userId, CommentType.COMMENT, page, pageSize);
    }

    @Override
    public PageInfo<Publish> queryAlbumList(Long userId, Integer page, Integer pageSize) {
        PageInfo<Publish> pageInfo = new PageInfo<>();
        pageInfo.setPageNub(page);
        pageInfo.setPageSize(pageSize);

        PageRequest pageRequest = PageRequest.of(page - 1, pageSize,
                Sort.by(Sort.Order.desc("created")));
        Query query = new Query().with(pageRequest);

        //查询自己的相册表
        List<PhotoAlbum> albumList = this.mongoTemplate.find(query, PhotoAlbum.class, "quanzi_album_" + userId);
        //如果返回值为空，说明没有找到。就是没有朋友发朋友圈。
        if (CollUtil.isEmpty(albumList)) {
            return pageInfo;
        }

        //然后去publish查找相关信息进行分装。
        List<Object> publishIdList = CollUtil.getFieldValues(albumList, "publishId");
        Query queryPublish = Query.query(Criteria.where("id").in(publishIdList))
                .with(Sort.by(Sort.Order.desc("created")));

        List<Publish> publishList = this.mongoTemplate.find(queryPublish, Publish.class);

        pageInfo.setRecommendUser(publishList);
        return pageInfo;
    }

    public PageInfo<Comment> queryCommentListByUser(Long userId, CommentType commentType, Integer page, Integer pageSize) {
        //1anma0：总体思路：直接到mongdb中，通过自己的id和时间倒序以及评论类型及逆行查询得到结果。
        PageInfo<Comment> pageInfo = new PageInfo<>();
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize,
                Sort.by(Sort.Order.desc("created")));
        Query query = Query.query(Criteria.where("publishUserId").is(userId).and("commentType").is(commentType.getType())).with(pageRequest);
        List<Comment> comments = mongoTemplate.find(query, Comment.class);
        pageInfo.setPageNub(page);
        pageInfo.setPageSize(pageSize);
        pageInfo.setRecommendUser(comments);
        return pageInfo;
    }


    public boolean saveComment(Long UserId, String publishId, CommentType type, String content) {
        try {
            Comment comment = new Comment();
            comment.setUserId(UserId);
            comment.setId(new ObjectId());
            comment.setCommentType(type.getType());
            comment.setContent(content);
            comment.setCreated(System.currentTimeMillis());
            comment.setPublishId(new ObjectId(publishId));
            Publish publish = queryPublishById(publishId);
            if (ObjectUtil.isNotEmpty(publish)) {
                comment.setPublishUserId(publish.getUserId());
            } else {
                Comment myComment = queryCommentById(publishId);
                if (ObjectUtil.isNotEmpty(myComment)) {
                    comment.setPublishUserId(myComment.getUserId());
                } else {
                    Video myVideo = videoApi.queryVideoById(publishId);
                    if (ObjectUtil.isNotEmpty(myVideo)) {
                        comment.setPublishUserId(myVideo.getUserId());
                    }
                }
            }
            mongoTemplate.save(comment);
            //redis中对应的评论数应该加1.


            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public Comment queryCommentById(String id) {
        return mongoTemplate.findById(new ObjectId(id), Comment.class);

    }

    public boolean removeComment(Long userId, String publishId, CommentType type) {
        try {
            //构建条件
            Query query = Query.query(Criteria.where("publishId").is(new ObjectId(publishId)).and("userId").is(userId).and("commentType").is(type.getType()));
            DeleteResult remove = mongoTemplate.remove(query, Comment.class);
            return remove.getDeletedCount() > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }


}