package com.tanhua.dubbo.server.enums;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/16
 * @描述
 */
public enum CommentType{

    LIKE(1), COMMENT(2), LOVE(3);

    int type;

    CommentType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
