package com.tanhua.dubbo.server.api;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.PageUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.mongodb.client.result.DeleteResult;
import com.tanhua.dubbo.server.enums.IdType;
import com.tanhua.dubbo.server.pojo.FollowUser;
import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.Video;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/18
 * @描述
 */

@Slf4j
@Service(version = "1.0.0")
public class VideoApiImpl implements VideoApi {
    @Autowired
    private IdService idService;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;



    private static final String VIDEO_FOLLOW_USER_KEY_PREFIX = "VIDEO_FOLLOW_USER_";

    @Override
    public String SaveVideo(Video video) {

        //判断送过来对象，是否有 url  picuil
        if (!ObjectUtil.isAllNotEmpty(video.getVideoUrl(), video.getPicUrl())) {
            return null;
        }
        try {
            //将数据填入video中
            video.setId(new ObjectId());
            video.setCreated(System.currentTimeMillis());
            video.setVid(idService.getAutoId(IdType.VIDEO));

            //保存到mondb
            mongoTemplate.save(video);
            return video.getId().toHexString();

        } catch (Exception e) {
            log.error("小视频发布失败~ video = " + video, e);
        }

        return null;
    }

    @Override
    public PageInfo<Video> queryVideoList(Long userId, Integer page, Integer pageSize) {
        PageInfo<Video> pageInfo = new PageInfo();
        pageInfo.setPageNub(page);
        pageInfo.setPageSize(pageSize);
        //1.从redis中拿到推荐的视频id;
        String redisKey = "QUANZI_VIDEO_RECOMMEND_" + userId;
        String recommendVideoIdStr = redisTemplate.opsForValue().get(redisKey);
        // "1,2,3,4"
        List<String> videoId = StrUtil.split(recommendVideoIdStr, ",");
        List<Long> recommendedVideo = new ArrayList<>();
        if (CollUtil.isNotEmpty(videoId)) {
            //设置分页。
            int[] ints = PageUtil.transToStartEnd(page - 1, pageSize);
            int start = ints[0];
            int end = Math.min(ints[1], videoId.size());

            for (int i = start; i < end; i++) {
                recommendedVideo.add(Convert.toLong(videoId.get(i)));
            }
        }
        else {
            //2.从视频列表里面按时间查找视频id
            int totalPage = PageUtil.totalPage(videoId.size(), pageSize);
            PageRequest pageRequest= PageRequest.of(page-1-totalPage,pageSize, Sort.by(Sort.Order.desc("created")));
            Query query = new Query().with(pageRequest);
            List<Video> videos = mongoTemplate.find(query, Video.class);
            pageInfo.setRecommendUser(videos);
            return pageInfo;
        }

        Query query = Query.query(Criteria.where("userId").in(recommendedVideo));
        List<Video> videos = mongoTemplate.find(query, Video.class);
        pageInfo.setRecommendUser(videos);
        return pageInfo;
    }

    @Override
    public Video queryVideoById(String videoId) {
        Video video = mongoTemplate.findById(new ObjectId(videoId), Video.class);
        return video;
    }

    @Override
    public Boolean isFollowUser(Long userId, Long followUserId) {
        String redisKey = this.getVideoFollowUserKey(userId);
        String hashKey = String.valueOf(followUserId);
        //1.查询redis中是否存在关注的缓冲
        Boolean aBoolean = this.redisTemplate.opsForHash().hasKey(redisKey, hashKey);
        if (aBoolean){
            return true;
        }
        //2.查询数据库是否关注.
        long count = mongoTemplate.count(Query.query(Criteria.where("userId").is(userId).and("followUserId").is(followUserId)), FollowUser.class);
        if (count==0){
            return false;
        }
        //3.将结果保存到redis中.
        redisTemplate.opsForHash().put(redisKey, hashKey,"1");
        return true;
    }

    private String getVideoFollowUserKey(Long userId) {
        return VIDEO_FOLLOW_USER_KEY_PREFIX+userId;
    }

    @Override
    public Boolean followUser(Long userId, Long followUserId) {
        //0.判断传入的数据 userId和followUserId是否为空
        if (!ObjectUtil.isAllNotEmpty(userId,followUserId)) {
            return false;
        }
        try {
            //1.查询是否关注
            Boolean isFollow = isFollowUser(userId, followUserId);
            if (isFollow){
                return false;
            }
            //2.如果没有关注,那就修改redis和monodb中的值
            FollowUser followUser = new FollowUser();
            followUser.setId(new ObjectId());
            followUser.setUserId(userId);
            followUser.setFollowUserId(followUserId);
            followUser.setCreated(System.currentTimeMillis());
            //保存到mongodb
            mongoTemplate.save(followUser);
            //保存到redis中
            String redisKey = this.getVideoFollowUserKey(userId);
            String hashKey = String.valueOf(followUserId);
            redisTemplate.opsForHash().put(redisKey, hashKey,"1");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public Boolean removeFollowUser(Long userId, Long followUserId) {
        //0.判断传入的数据 userId和followUserId是否为空
        if (!ObjectUtil.isAllNotEmpty(userId,followUserId)) {
            return false;
        }

        try {
            //1.查询是否关注
            Boolean isFollow = isFollowUser(userId, followUserId);
            if (!isFollow){
                return false;
            }
            //取消关注，删除关注数据即可
            Query query = Query.query(Criteria.where("userId").is(userId)
                    .and("followUserId").is(followUserId)
            );
            DeleteResult result = this.mongoTemplate.remove(query, FollowUser.class);
            if (result.getDeletedCount() > 0) {
                //同时删除redis中的数据
                String redisKey = this.getVideoFollowUserKey(userId);
                String hashKey = String.valueOf(followUserId);
                this.redisTemplate.opsForHash().delete(redisKey, hashKey);

                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }


}
