package com.tanhua.dubbo.server.api;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.convert.Convert;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.pojo.UserLike;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/22
 * @描述
 */
@Service(version = "1.0.0")
public class UserLikeApiImpl implements UserLikeApi {

    public static final String LIKE_REDIS_KEY_PREFIX = "USER_LIKE_";
    public static final String NOT_LIKE_REDIS_KEY_PREFIX = "USER_NOT_LIKE_";
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    //获取 喜欢 数据的redis key
    private String getLikeRedisKey(Long userId) {
        return LIKE_REDIS_KEY_PREFIX + userId;
    }

    // 获取 不喜欢 数据的redis key

    private String getNotLikeRedisKey(Long userId) {
        return NOT_LIKE_REDIS_KEY_PREFIX + userId;
    }


    @Override
    public Boolean likeUser(Long userId, Long likeUserId) {
        //1.先判断是否是喜欢。如果已经喜欢了，直接返回fasle
        if (this.isLike(userId, likeUserId)) {
            return false;
        }
        //2.修改mondb的数据
        UserLike userLike = new UserLike();
        userLike.setCreated(System.currentTimeMillis());
        userLike.setId(new ObjectId());
        userLike.setUserId(userId);
        userLike.setLikeUserId(likeUserId);
        mongoTemplate.save(userLike);


        //3.更新redis中数据
        String redisKey = this.getLikeRedisKey(userId);
        String hashKey = String.valueOf(likeUserId);
        this.redisTemplate.opsForHash().put(redisKey, hashKey, "1");

        //4.喜欢的用户是否再不喜欢的列表中。如果有那么要删除。(不喜欢列表没有持久化)


        if (isNotLike(userId, likeUserId)) {
            redisKey = this.getNotLikeRedisKey(userId);
            this.redisTemplate.opsForHash().delete(redisKey, hashKey);
        }


        return true;
    }

    public Boolean isLike(Long userId, Long likeUserId) {
        String redisKey = this.getLikeRedisKey(userId);
        String hashKey = String.valueOf(likeUserId);
        return redisTemplate.opsForHash().hasKey(redisKey, hashKey);
    }


    @Override
    public Boolean notLikeUser(Long userId, Long likeUserId) {
        //1.判断是否不喜欢
        if (this.isNotLike(userId, likeUserId)) {
            return false;
        }
        //2将不喜欢的数据添加到redis中.  这里不喜欢的数据没有持久化.
        String redisKey = this.getNotLikeRedisKey(userId);
        String hashKey = String.valueOf(likeUserId);
        this.redisTemplate.opsForHash().put(redisKey, hashKey, "1");

        //3.判断喜欢列表是否拥有.如果有就删除.
        if (this.isLike(userId, likeUserId)) {
            //删除MongoDB数据
            Query query = Query.query(Criteria
                    .where("userId").is(userId)
                    .and("likeUserId").is(likeUserId)
            );
            this.mongoTemplate.remove(query, UserLike.class);

            //删除redis中的数据
            redisKey = this.getLikeRedisKey(userId);
            this.redisTemplate.opsForHash().delete(redisKey, hashKey);
        }
        return true;


    }

    public Boolean isNotLike(Long userId, Long likeUserId) {
        String redisKey = this.getNotLikeRedisKey(userId);
        String hashKey = String.valueOf(likeUserId);
        return redisTemplate.opsForHash().hasKey(redisKey, hashKey);
    }

    @Override
    public Boolean isMutualLike(Long userId, Long likeUserId) {
        return isLike(userId, likeUserId) && isLike(likeUserId, userId);
    }

    @Override
    public List<Long> queryLikeList(Long userId) {
        //直接通过redis查询数据
        String redisKey = this.getLikeRedisKey(userId);
        //通过大key 可以得到所有的小key  也是就细喜欢的对象.
        Set<Object> keys = this.redisTemplate.opsForHash().keys(redisKey);
        if (CollUtil.isEmpty(keys)) {
            return ListUtil.empty();
        }
        //将结果封装到list中.
        List<Long> result = new ArrayList<>(keys.size());
        keys.forEach(o -> result.add(Convert.toLong(o)));
        return result;
    }

    @Override
    public List<Long> queryNotLikeList(Long userId) {
        //直接通过redis查询数据
        String redisKey = this.getNotLikeRedisKey(userId);
        //通过大key 可以得到所有的小key  也是就细喜欢的对象.
        Set<Object> keys = this.redisTemplate.opsForHash().keys(redisKey);
        if (CollUtil.isEmpty(keys)) {
            return ListUtil.empty();
        }
        //将结果封装到list中.
        List<Long> result = new ArrayList<>(keys.size());
        keys.forEach(o -> result.add(Convert.toLong(o)));
        return result;
    }

    @Override
    public Long queryMutualLikeCount(Long userId) {
        //1.先查询我的喜欢列表
        List<Long> likeList = queryLikeList(userId);

        //1.通过我喜欢的人.查询是否喜欢我.
        Long count = 0L;
        for (Long like : likeList) {
            String redisKey = getLikeRedisKey(like);
            String hashKey = String.valueOf(userId);

            if (redisTemplate.opsForHash().hasKey(redisKey, hashKey)) {
                count++;
            }
        }

        return count;
    }

    @Override
    public Long queryLikeCount(Long userId) {
//        List<Long> likeList = queryLikeList(userId);
//        return Long.valueOf(likeList.size());
        //从redis中查.
        String redisKey = getLikeRedisKey(userId);
        return this.redisTemplate.opsForHash().size(redisKey);
    }

    @Override
    //喜欢我的数;
    public Long queryFanCount(Long userId) {
        Query likeUserId = Query.query(Criteria.where("likeUserId").in(userId));
        long count = mongoTemplate.count(likeUserId, UserLike.class);
        return count;
    }
}
