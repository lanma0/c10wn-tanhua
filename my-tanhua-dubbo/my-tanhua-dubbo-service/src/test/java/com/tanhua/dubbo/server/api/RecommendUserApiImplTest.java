package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.RecommendUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/12
 * @描述
 */

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class RecommendUserApiImplTest {

    @Autowired
    RecommendUserApiImpl recommendUserApi;

    @Test
    public void getMaxScoreUser() {
        RecommendUser maxScoreUser = recommendUserApi.getMaxScoreUser(1L);
        System.out.println(maxScoreUser);
    }

    @Test
    public void getUserPageInfo() {
        PageInfo<RecommendUser> userPageInfo = recommendUserApi.getUserPageInfo(1L, 1, 10);
        System.out.println(userPageInfo);
    }
}