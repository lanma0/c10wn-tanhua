package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.Publish;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/14
 * @描述
 */

@SpringBootTest
@RunWith(SpringRunner.class)
public class QuanziApiImplTest {
    @Autowired
    private QuanZiApi quanZiApi;

    @Test
    public void queryPublishList() {
        PageInfo<Publish> pageInfo = this.quanZiApi.queryPublishList(1, 10, 2L);
        pageInfo.getRecommendUser().forEach(System.out::println);
        System.out.println("------------");
//        this.quanZiApi.queryPublishList(2, 2, 1L)
//                .getRecommendUser().forEach(publish -> System.out.println(publish));
//        System.out.println("------------");
//        this.quanZiApi.queryPublishList(1, 3, 2L)
//                .getRecommendUser().forEach(publish -> System.out.println(publish));
    }


}