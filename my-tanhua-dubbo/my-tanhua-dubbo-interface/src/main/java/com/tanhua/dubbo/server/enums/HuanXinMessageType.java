package com.tanhua.dubbo.server.enums;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/23
 * @描述
 */
public enum HuanXinMessageType {

    TXT("txt"), IMG("img"), LOC("loc"), AUDIO("audio"), VIDEO("video"), FILE("file");

    String type ;

    HuanXinMessageType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
