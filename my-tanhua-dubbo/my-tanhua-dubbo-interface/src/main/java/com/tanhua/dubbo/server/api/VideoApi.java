package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.Video;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/18
 * @描述
 */
public interface VideoApi {

    //-----------------------视频保存和查询相关接口开发

    String SaveVideo(Video video);

    PageInfo<Video> queryVideoList(Long userId, Integer page, Integer pageSize);


    Video queryVideoById(String videoId);


    //---------------------关注相关接口开发----------------

    //1.是否关注
    Boolean isFollowUser(Long userId,Long followUserId);
    //2.关注操作
    Boolean followUser(Long userId,Long followUserId);
    //3.取消关注操作
    Boolean removeFollowUser(Long userId,Long followUserId);

}
