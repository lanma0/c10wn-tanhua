package com.tanhua.dubbo.server.vo;

import cn.hutool.core.bean.BeanUtil;
import com.tanhua.dubbo.server.pojo.UserLocation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/22
 * @描述
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserLocationVo implements Serializable {

    private static final long serialVersionUID = 4133419501260037769L;
    private Long userId; //用户id
    private Double longitude; //经度
    private Double latitude; //维度
    private String address; //位置描述
    private Long created; //创建时间
    private Long updated; //更新时间
    private Long lastUpdated; //上次更新时间

    public static final UserLocationVo format(UserLocation userLocation) {
        //将相同的东西进行封装.
        UserLocationVo userLocationVo = BeanUtil.toBean(userLocation, UserLocationVo.class);
        //将经纬度进行分解.成为2个Long.这样就可以再网络上传输了.
        userLocationVo.setLongitude(userLocation.getLocation().getLon());
        userLocationVo.setLatitude(userLocation.getLocation().getLat());
        return userLocationVo;
    }

    //批量处理上面的UserLocal
    public static final List<UserLocationVo> formatToList(List<UserLocation> userLocations) {
        List<UserLocationVo> list = new ArrayList<>();
        for (UserLocation userLocation : userLocations) {
            list.add(format(userLocation));
        }
        return list;
    }

}
