package com.tanhua.dubbo.server.api;


import com.tanhua.common.pojo.HuanXinUser;
import com.tanhua.dubbo.server.enums.HuanXinMessageType;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/20
 * @描述
 */
public interface HuanXinApi {


    /**
     * 获得tokenapi，需要查看他人文档要求。
     * @return
     */
    String getToken();

    Boolean register(Long userId);


    //查询环信用户，通过ID
    HuanXinUser queryHuanXinUser(Long userId);

    //通过环信用户的账号信息
    HuanXinUser queryUserByUserName(String userName);


    /**
     * 添加好友（双向好友关系）
     * 参见：http://docs-im.easemob.com/im/server/ready/user#%E6%B7%BB%E5%8A%A0%E5%A5%BD%E5%8F%8B
     *
     * @param userId   自己的id
     * @param friendId 好友的id
     * @return
     */
    Boolean addUserFriend(Long userId, Long friendId);

    /**
     * 删除好友关系（双向删除）
     * 参见：http://docs-im.easemob.com/im/server/ready/user#%E7%A7%BB%E9%99%A4%E5%A5%BD%E5%8F%8B
     *
     * @param userId   自己的id
     * @param friendId 好友的id
     * @return
     */
    Boolean removeUserFriend(Long userId, Long friendId);

    /**
     * 以管理员身份发送消息
     * 文档地址：http://docs-im.easemob.com/im/server/basics/messages#%E5%8F%91%E9%80%81%E6%B6%88%E6%81%AF
     *
     * @param targetUserName 发送目标的用户名
     * @param huanXinMessageType 消息类型
     * @param msg
     * @return
     */
    Boolean sendMsgFromAdmin(String targetUserName, HuanXinMessageType huanXinMessageType, String msg);


}
