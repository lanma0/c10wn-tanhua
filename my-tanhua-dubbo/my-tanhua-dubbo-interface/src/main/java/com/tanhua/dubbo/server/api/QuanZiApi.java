package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.Comment;
import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.Publish;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/14
 * @描述 通过分页方法查询圈子的方法；
 */
public interface QuanZiApi {

   /**
    * 查询好友动态
    *
    * @param userId 用户id
    * @param page 当前页数
    * @param pagesize 每一页查询的数据条数
    * @return
    */
   PageInfo<Publish> queryPublishList(Integer page,Integer pagesize,Long userId);


   PageInfo<Publish> queryRecommendPublishList(Integer page,Integer pagesize,Long userId);

   /**
    * 发布动态
    *
    * @param publish
    * @return 发布成功返回动态id
    */
   String savePublish(Publish publish);



   //----------------------圈子comment相关的方法（点赞）--------------------------
   Publish queryPublishById(String id);

   //1.点赞
   boolean likeComment(Long UserId,String publishId);

   //2.取消点赞
   boolean removeLikeComment(Long UserId,String publishId);

   //3.查看点赞数量
   Long getLikeCount(String publishId);

   //4.查看用户是否点赞
   boolean UserIsLike(Long UserId,String publishId);

   //----------------------圈子comment相关的方法（喜欢）--------------------------

   boolean loveComment(Long UserId,String publishId);

   boolean removeLoveComment(Long UserId,String publishId);

   //3.查看点赞数量
   Long getLoveCount(String publishId);

   //4.查看用户是否点赞
   boolean UserIsLove(Long UserId,String publishId);



   //----------------------圈子comment相关的方法（评论）--------------------------


   //查询评论列表
   PageInfo<Comment>  queryCommentList(String publishId,Integer page,Integer pageSize);

   Boolean saveComment(String publishId ,Long userId,String content);

   Long getCommentCount(String publishId);

   //查看与我相关的  点赞，评论和喜欢，结果都用 pageInfo来分装。
   /**
    * 查询对我的点赞消息列表
    *
    * @return
    */
   PageInfo<Comment> queryLikeCommentListByUser(Long userId, Integer page, Integer pageSize);

   /**
    * 查询对我的喜欢消息列表
    *
    * @return
    */
   PageInfo<Comment> queryLoveCommentListByUser(Long userId, Integer page, Integer pageSize);

   /**
    * 查询对我的评论消息列表
    *
    * @return
    */
   PageInfo<Comment> queryCommentListByUser(Long userId, Integer page, Integer pageSize);

   /**
    * 查询相册表
    *
    * @param userId
    * @param page
    * @param pageSize
    * @return
    */
   PageInfo<Publish> queryAlbumList(Long userId, Integer page, Integer pageSize);


}
