package com.tanhua.dubbo.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/12
 * @描述
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageInfo<E> implements Serializable {

    private static final long serialVersionUID = -2105385689859184204L;

    private Integer total=0; //命中数
    private Integer pageNub = 0; //当前页
    private Integer pageSize=0; //分页大小
    private List<E> recommendUser = Collections.emptyList(); //创建一个空集合：用来接受recommendUser结合
}
