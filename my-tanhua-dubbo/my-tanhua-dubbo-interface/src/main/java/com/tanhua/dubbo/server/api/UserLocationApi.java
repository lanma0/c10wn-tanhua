package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.UserLocation;
import com.tanhua.dubbo.server.vo.UserLocationVo;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/22
 * @描述
 */
public interface UserLocationApi {

    /**
     * 更新用户地理位置
     *
     * @param userId 用户id
     * @param longitude 经度
     * @param latitude 纬度
     * @param address 地址名称
     * @return
     */
    Boolean updateUserLocation(Long userId, Double longitude, Double latitude, String address);

    //通过id查找UserLocationVo对象
    UserLocationVo queryByUserId(Long userId);


    //查询附近好友的方法
    PageInfo<UserLocationVo> queryUserFromLocation(Double longitude, Double latitude, Double distance, Integer page, Integer pageSize);

}
