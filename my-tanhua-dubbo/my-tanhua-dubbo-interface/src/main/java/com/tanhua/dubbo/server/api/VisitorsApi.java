package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.Visitors;

import java.util.List;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/23
 * @描述
 */
public interface VisitorsApi {

    /**
     *
     * @param userId 我的id
     * @param visitorUserId 来访者的id
     * @param from 来源
     * @return
     */

    public String saveVisitor(Long userId, Long visitorUserId, String from);
    /**
     * 查询我的访客数据，存在2种情况：
     * 1. 没有看过我的访客数据，返回前5个访客信息
     * 2. 之前看过我的访客，从上一次查看的时间点往后查询5个访客数据
     *
     * @param userId
     * @return
     */

    List<Visitors> queryMyVisitor(Long userId);

    /**
     * 按照时间倒序排序，查询最近的访客信息
     *
     * @param userId
     * @param page
     * @param pageSize
     * @return
     */
    PageInfo<Visitors> queryTopVisitor(Long userId, Integer page, Integer pageSize);
}
