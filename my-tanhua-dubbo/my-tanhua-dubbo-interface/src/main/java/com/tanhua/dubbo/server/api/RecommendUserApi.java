package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.RecommendUser;

import java.util.List;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/12
 * @描述 接口服务：获得今日佳人，和推荐好友
 */
public interface RecommendUserApi {


    RecommendUser getMaxScoreUser(Long userId);
    PageInfo<RecommendUser> getUserPageInfo(Long userId, Integer pageNum, Integer pageSize);

    /**
     * 查询推荐好友的缘分值
     *
     * @param userId 好友的id
     * @param toUserId 我的id
     * @return
     */
    Double queryScore(Long userId, Long toUserId);

    List<RecommendUser> queryCardList(Long userId, Integer count);
}
