package com.tanhua.dubbo.server.api;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.pojo.PageInfo;
import com.tanhua.dubbo.server.pojo.UserLocation;
import com.tanhua.dubbo.server.vo.UserLocationVo;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.GeoDistanceQueryBuilder;
import org.elasticsearch.search.sort.GeoDistanceSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.data.elasticsearch.core.query.*;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/22
 * @描述
 */

@Service(version = "1.0.0")
@Slf4j
public class UserLocationApiImpl implements UserLocationApi {
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    //初始化数据库

    @PostConstruct
    public void init() {
        //1.判断elastic的索引是否存在
        if (!elasticsearchTemplate.indexExists(UserLocation.class)) {
            elasticsearchTemplate.createIndex(UserLocation.class);
        }
        //2.判断对应的docute是否存在
        if (!elasticsearchTemplate.typeExists("tanhua", "user_location")) {
            elasticsearchTemplate.putMapping(UserLocation.class);
        }
    }


    @Override
    public Boolean updateUserLocation(Long userId, Double longitude, Double latitude, String address) {


        //1.判断数据库中是否已经存在 对应的经纬度.如果存在,那么更新.如果不存在那么新增.
        try {
            GetQuery getQuery = new GetQuery();
            getQuery.setId(String.valueOf(userId));
            UserLocation userLocation = elasticsearchTemplate.queryForObject(getQuery, UserLocation.class);

            if (ObjectUtil.isEmpty(userLocation)) {
                userLocation.setUserId(userId);
                userLocation.setAddress(address);
                userLocation.setCreated(System.currentTimeMillis());
                userLocation.setUpdated(userLocation.getCreated());
                userLocation.setLastUpdated(userLocation.getCreated());
                userLocation.setLocation(new GeoPoint(latitude, longitude));

                IndexQuery indexQuery = new IndexQueryBuilder().withObject(userLocation).build();

                elasticsearchTemplate.index(indexQuery);

            } else {
                //更新
                UpdateRequest updateRequest = new UpdateRequest();
                Map<String, Object> map = new HashMap<>();
                map.put("location", new GeoPoint(latitude, longitude));
                map.put("updated", System.currentTimeMillis());
                map.put("lastUpdated", userLocation.getUpdated());
                map.put("address", address);
                updateRequest.doc(map);
                UpdateQuery updateQuery = new UpdateQueryBuilder()
                        .withId(String.valueOf(userId)) //确定id位置
                        .withClass(UserLocation.class)   //确定类型
                        .withUpdateRequest(updateRequest).build();//隐射修改数据.
                elasticsearchTemplate.update(updateQuery);
            }
            return true;
        } catch (Exception e) {
            log.error("更新地理位置失败~ userId = " + userId + ", longitude = " + longitude + ", latitude = " + latitude + ", address = " + address, e);
            return false;
        }
    }

    @Override
    public UserLocationVo queryByUserId(Long userId) {
        GetQuery getQuery = new GetQuery();
        getQuery.setId(String.valueOf(userId));
        UserLocation userLocation = elasticsearchTemplate.queryForObject(getQuery, UserLocation.class);
        if (ObjectUtil.isNotEmpty(userLocation)) {
            return UserLocationVo.format(userLocation);
        }
        return null;
    }

    /**
     * 根据位置搜索
     *
     * @param longitude 经度
     * @param latitude  纬度
     * @param distance  距离(米)
     * @param page      页数
     * @param pageSize  页面大小
     */
    @Override
    public PageInfo<UserLocationVo> queryUserFromLocation(Double longitude, Double latitude, Double distance, Integer page, Integer pageSize) {
        //1.创建pageinfo并设置相关参数
        PageInfo<UserLocationVo> pageInfo = new PageInfo<>();
        pageInfo.setPageNub(page);
        pageInfo.setPageSize(pageSize);

        //elastic查询
        //分页
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize);

        //实现了SearchQuery接口，构造分页、排序
        NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder();

        searchQueryBuilder.withPageable(pageRequest);


//bool 多条件精确查询.
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        GeoDistanceQueryBuilder geoDistanceQueryBuilder = new GeoDistanceQueryBuilder("location");

        //
        geoDistanceQueryBuilder.distance(distance / 10000, DistanceUnit.KILOMETERS);
        geoDistanceQueryBuilder.point(new org.elasticsearch.common.geo.GeoPoint(latitude, longitude));
        boolQueryBuilder.must(geoDistanceQueryBuilder);
        searchQueryBuilder.withQuery(boolQueryBuilder);


        //拍寻
        GeoDistanceSortBuilder geoDistanceSortBuilder = new GeoDistanceSortBuilder("location", latitude, longitude);
        geoDistanceSortBuilder.order(SortOrder.ASC); //正序排序
        geoDistanceSortBuilder.unit(DistanceUnit.KILOMETERS); //设置单位
        searchQueryBuilder.withSort(geoDistanceSortBuilder);


        AggregatedPage<UserLocation> userLocations = elasticsearchTemplate.queryForPage(searchQueryBuilder.build(), UserLocation.class);
        if (ObjectUtil.isNotEmpty(userLocations)) {
            pageInfo.setRecommendUser(UserLocationVo.formatToList(userLocations.getContent()));
        }
        return pageInfo;
    }
}
