package com.tanhua.sso.other;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.PutObjectRequest;
import com.tanhua.common.config.AliyunConfig;
import com.tanhua.common.service.BuddhaLoadService;


import com.tanhua.sso.service.FaceCheckService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.util.Date;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/9
 * @描述
 */

@SpringBootTest

@RunWith(SpringJUnit4ClassRunner.class)
public class OtherTest {
    @Autowired
    private OSSClient ossClient;
    @Autowired
    private AliyunConfig aliyunConfig;
    @Test
    public void  test1(){
        Date date = new Date();
        PutObjectRequest putObjectRequest = new PutObjectRequest(aliyunConfig.getBucketName(),"tanhua/oos/1.jpg", new File("D:\\360MoveData\\Users\\86138\\Desktop\\探花\\day01-项目介绍以及实现登录功能\\资料\\测试图片\\1.jpg"));

// 上传文件。
        ossClient.putObject(putObjectRequest);

// 关闭OSSClient。
        ossClient.shutdown();
    }

    @Test
    public void  test2(){
        BuddhaLoadService buddhaLoadService = new BuddhaLoadService();
        String filePath = buddhaLoadService.getFilePath("1.jpg");
        System.out.println("filePath = " + filePath);

    }
    @Test
    public void  test3(){
        FaceCheckService checkService = new FaceCheckService();
        File file =new File("D:\\360MoveData\\Users\\86138\\Desktop\\测试图片\\t.jpg") ;
        System.out.println("file.getName() = " + file.getName());
        System.out.println("file.getAbsolutePath() = " + file.getAbsolutePath());
        checkService.isPerson(file);


    }




}
