package com.tanhua.sso.service;


import com.tanhua.sso.util.RedisUtil;
import com.tanhua.sso.vo.ErrorResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SmsServiceTest {

//    @Resource
//    RedisTemplate<String,Object> redisTemplate;

    @Autowired
    private SmsService smsService;
    @Autowired
    private RedisUtil redisUtil;
    @Test
    public void test1(){
//        String s = smsService.sendSms("13873377952");
//        System.out.println(s);
        ErrorResult errorResult = smsService.sendChekCode("878403789@qq.com");
        System.out.println(errorResult);
    }
    @Test
    public void test2(){
//        boolean wangnu = redisUtil.set("wangnu", "bbs");
//        redisTemplate.
//        String test = (String) redisUtil.get("wangnu");
//        System.out.println("test = " + test);

//        System.out.println(wangnu);
        long expire = redisUtil.getExpire("CHECK_CODE_878403789@qq.com");
        System.out.println("expire = " + expire);
    }
    @Test
    public void  test3(){
        String s = smsService.sendMail("878403789@qq.com");
        System.out.println(s);
    }

    @Test
    public void test5(){
        redisUtil.set("CHECK_CODE_17602026868","123456");
        String check_code_17602026868 = (String) redisUtil.get("CHECK_CODE_17602026868");
        System.out.println(check_code_17602026868);

    }

}
