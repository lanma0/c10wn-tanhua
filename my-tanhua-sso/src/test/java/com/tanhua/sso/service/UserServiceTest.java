package com.tanhua.sso.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.tanhua.common.mapper.UserMapper;
import com.tanhua.common.pojo.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserServiceTest {
    String secret = "76bd425b6f29f7fcc2e0bfc286043df1";
    //    @Autowired
//    private RedisTemplate<String, String> redisTemplate;
    @Autowired
   private UserMapper userMapper;
    @Autowired
    private UserService userService;


    @Test
    public void test1() {
        //jwt 第一块加密方式选择信息
        Map<String, Object> mapHeader = new HashMap<>();

           /* {
                "alg": "HS256",
                    "typ": "JWT"
            }*/

        mapHeader.put(JwsHeader.TYPE, JwsHeader.JWT_TYPE);//typ   JWT
        mapHeader.put(JwsHeader.ALGORITHM, "HS256");//alg
        Map<String, Object> mapClaims = new HashMap<>();

        //jwt第二块用户信息；
//        mapClaims.put("mobile", "878403789@qq.com");
        mapClaims.put("id", "1413305868119752705");
        //配置jwt的信息，sigWith为加密的盐值；
        String jwt = Jwts.builder().setHeader(mapHeader).setClaims(mapClaims).signWith(SignatureAlgorithm.HS256, secret).setExpiration(new Date(System.currentTimeMillis() + 3000 * 200)).compact();
        System.out.println(jwt);

    }

    @Test
    public void test2() {
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjE0MTMzMDU4NjgxMTk3NTI3MDUiLCJleHAiOjE2MjU5MTE5ODR9.AUym9RhiPnHoD-tEfPHt6FMco4P1xcYOLz6WHgyDT6U";

//            String token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtb2JpbGUiOiI4Nzg0MDM3ODlAcXEuY29tIiwiaWQiOiIyIiwiZXhwIjoxNjI1NzM0NzYxfQ.q3eNLrQMOxayw3ONj_tYO28RAsJWH7Hltx-uvJELyww";
//        Map<String, Object> body = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
//        System.out.println(body);
        User user = userService.chekToken(token);
        System.out.println(user);


    }

    @Test
    public void test3() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("account", "3031562962@qq.com");

        User user = userMapper.selectOne(queryWrapper);
        if (null == user) {
            //需要注册该用户
            user = new User();
            user.setAccount("3031562962@qq.com");
            user.setPassword(DigestUtils.md5Hex("123456"));
            //注册新用户
            this.userMapper.insert(user);
        }

        //生成token
        Map<String, Object> claims = new HashMap<String, Object>();
        claims.put("id", user.getId());
        System.out.println(claims);
    }
    @Test
    public void test4(){
        Calendar now = Calendar.getInstance();
        now.add(Calendar.HOUR,12);
        System.out.println(now.getTime().getTime());
    }
    @Test
    public void test5(){
        String login = userService.login("878403789@qq.com", "264171");

    }


}
