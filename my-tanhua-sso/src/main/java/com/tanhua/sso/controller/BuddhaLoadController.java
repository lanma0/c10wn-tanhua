package com.tanhua.sso.controller;

import com.tanhua.common.service.BuddhaLoadService;
import com.tanhua.common.vo.BuddhaLoadResoult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/10
 * @描述 头像上传的Controller
 */

@RestController
@RequestMapping("bunddha")
public class BuddhaLoadController {


    @Autowired
    private BuddhaLoadService buddhaLoadService;

    @RequestMapping("upload")
    //用uploadFile绑定前端传过来的file文件
    public BuddhaLoadResoult upload(@RequestParam("file") MultipartFile uploadFile) {

        BuddhaLoadResoult buddhaLoadResoult = buddhaLoadService.buddhaLoad(uploadFile);

        return buddhaLoadResoult;

    }

}
