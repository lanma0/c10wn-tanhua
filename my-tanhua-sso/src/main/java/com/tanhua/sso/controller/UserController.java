package com.tanhua.sso.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanhua.common.pojo.User;
import com.tanhua.sso.service.SmsService;
import com.tanhua.sso.service.UserService;
import com.tanhua.sso.vo.ErrorResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RequestMapping("user")
@RestController
public class UserController {
    @Autowired
    private SmsService smsService;
    @Autowired
    private UserService userServic;

    @PostMapping("login")
    //ResponseEntity标识整个http相应：状态码、头部信息以及相应体内容。因此我们可以使用其对http响应实现完整配置。
    public ResponseEntity sendCheckCode(@RequestBody Map<String, String> loginmap) {

        ErrorResult errorResult = null;
        try {
            String phone = loginmap.get("phone");
            errorResult = smsService.sendChekCode(phone);
            if (null == errorResult) {
                return ResponseEntity.ok(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            ErrorResult.builder().errCode("0000002").errMessage("短信码发送失败").build();
        }
        //
        return ResponseEntity.status(500).body(errorResult);

    }

    @PostMapping("loginVerification")
    public ResponseEntity login(@RequestBody Map<String, String> param) {
        String phone = param.get("phone");
        String code = param.get("verificationCode");

        String login = userServic.login(phone, code);

        if (StringUtils.isNotEmpty(login)) {
            try {
                Map<String, Object> result = new HashMap<>(2);
                String[] split = StringUtils.split(login,",");
                result.put("token", split[0]);
                result.put("isNew", Boolean.valueOf(split[1]));

                return ResponseEntity.ok(result);
            } catch (Exception  e) {
                e.printStackTrace();
            }
        }
        ErrorResult loginError = ErrorResult.builder().errMessage("登录失败").errCode("000002").build();
        return ResponseEntity.status(500).body(loginError);

    }
    @GetMapping("/{token}")
    public String chekToken(@PathVariable String token){

        ObjectMapper objectMapper = new ObjectMapper();
        String resoult = null;
        try {
            User user = userServic.chekToken(token);
            resoult = objectMapper.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return resoult;

    }
}
