package com.tanhua.sso.controller;

import com.tanhua.sso.service.UserInfoService;
import com.tanhua.sso.vo.ErrorResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/11
 * @描述 用于用户信息和头像上传
 */

@Controller
@RequestMapping("user")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;
    /**
     *@描述 PerfectInfo :用户上传信息的controller
     *@参数 [params:用户上传的参数, token:登录后携带的token]
     *@返回值 org.springframework.http.ResponseEntity:状态码和消息体
     *@创建人  1anma0
     *@创建时间 2021/7/11
     *@修改人和其它信息
     */
    @PostMapping("info")
    public ResponseEntity PerfectInfo(@RequestBody Map<String, String> params, @RequestHeader("Authorization") String token) {

        try {
            //调用service上传信息.
            Boolean aBoolean = userInfoService.savaUserInfo(params, token);
            if (aBoolean) {
                //说明上传成功,返回200
                return ResponseEntity.ok(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //上传失败,返回失败状态码500,和消息体
        ErrorResult build = ErrorResult.builder().errCode("0000001").errMessage("保存信息失败!").build();
        return ResponseEntity.status(500).body(build);
    }
    @PostMapping("head")
    public ResponseEntity uploadBuddha(@RequestParam("header") MultipartFile file, @RequestHeader("Authorization") String token) {
        try {
            Boolean aBoolean = userInfoService.saveBuddha(file, token);
            if (aBoolean) {
                return ResponseEntity.ok(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ErrorResult build = ErrorResult.builder().errCode("0000002").errMessage("头像保存失败!").build();
        return ResponseEntity.status(500).body(build);

    }


}
