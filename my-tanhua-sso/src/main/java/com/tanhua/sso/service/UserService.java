package com.tanhua.sso.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.common.mapper.UserMapper;
import com.tanhua.common.pojo.User;
import com.tanhua.dubbo.server.api.HuanXinApi;
import com.tanhua.sso.util.RedisUtil;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Slf4j
@Service
public class UserService {

    //导入RedisTeplate
    @Autowired
    private RedisUtil redisUtil;

    //导入UserMapper
    @Autowired
    private UserMapper userMapper;

    //设置加密盐值 sercet
    @Value("${jwt.secret}")
    private String sercet;



    //mq消息队列
    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Reference(version = "1.0.0")
    private HuanXinApi huanXinApi;
    //校验验证码

    /**
     * @描述 login 用户登录
     * @参数 [v1 手机号或者邮箱号, code 验证码]
     * @返回值 java.lang.String：token+是否为新用户
     * @创建人 1anma0
     * @创建时间 2021/7/8
     * @修改人和其它信息
     */
    public String login(String v1, String code) {
        //设置一个变量判断是否为新用户isNew
        boolean isNew = false;

        String rediskey = "CHECK_CODE_" + v1;
        String redisCode = (String)redisUtil.get(rediskey);
        if (!StringUtils.equals(redisCode,code)) {
            return null;
        }
        //验证码在校验完成后，需要废弃
        redisUtil.del(rediskey);
        //连接数据库查看是否存在数据库
        QueryWrapper<User> qw = new QueryWrapper<>();
        qw.eq("account", v1);
        User user = userMapper.selectOne(qw);
        //如果不是新用户，那么创建对象，并添加到数据库；

        if (null == user) {
            //需要注册该用户
            user = new User();
            user.setAccount(v1);
            user.setPassword(DigestUtils.md5Hex("123456"));
            //添加到数据库
            userMapper.insert(user);
            isNew = true;

            //注册环信用户
            Boolean register = huanXinApi.register(user.getId());
            if (!register){
        //注册失败,
                log.error("注册环信用户失败~ userId = " + user.getId());
            }


        }
        //生成token
        Map<String, Object> mapHeader = new HashMap<>();
        mapHeader.put("alg", "HS256");
        mapHeader.put("typ", "JWT");


        Map<String, Object> mapPlayLoad = new HashMap<>();
        mapPlayLoad.put("account", user.getAccount());
        mapPlayLoad.put("id", user.getId().toString());


        //生成token
        //生成token有效时间
        Calendar now = Calendar.getInstance();
        now.add(Calendar.HOUR, 12);
        String token = Jwts.builder().setHeader(mapHeader).setClaims(mapPlayLoad).signWith(SignatureAlgorithm.HS256, sercet).setExpiration(now.getTime()).compact();

        //发送登录成功的消息消息

        try {
            Map<String, Object> mqmsg = new HashMap<>();
            mqmsg.put("id", user.getId());
            mqmsg.put("date", System.currentTimeMillis());

            rocketMQTemplate.convertAndSend("tanhua-sso-login", mqmsg);
        } catch (MessagingException e) {
            e.printStackTrace();
            log.error("发送消息失败！", e);
        }
        return token + "," + isNew;

    }
    //校验token
    public User chekToken(String token){
        try {
            Map<String,Object> body = Jwts.parser().setSigningKey(sercet).parseClaimsJws(token).getBody();
            User user = new User();
            String id = (String) body.get("id");
            user.setId(Long.parseLong(id));
            String redisKey = "TANHUA_USER_MOBILE_" + user.getId();
            //查询redis中是否存在账号,如果存在,就放到user中.
            //如果不存在,再去数据库查询User.减少数据库的查询压力.
            if (redisUtil.hasKey(redisKey)) {
                String account = (String) redisUtil.get(redisKey);
                user.setAccount(account);
            }else {
                //从数据中查询对象;
                User sUser = userMapper.selectById(id);
                user.setAccount(sUser.getAccount());
                //获取token的剩余有效时间
                long tokenTime = Long.parseLong(body.get("exp").toString());
                long exp = tokenTime * 1000 - System.currentTimeMillis();
                redisUtil.set(redisKey,sUser.getId(),exp);
                redisUtil.set(redisKey,sUser.getAccount(),exp);
            }
            return user;
        } catch (ExpiredJwtException e) {
            e.printStackTrace();
            log.info("token已经过期！ token = " + token);
        } catch (Exception e) {
            log.info("token错误！ token = " + token,e);
        }
        return null ;


    }


    //发送用户登录成功的消息
}
