package com.tanhua.sso.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.common.mapper.UserInfoMapper;
import com.tanhua.common.pojo.User;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.common.service.BuddhaLoadService;
import com.tanhua.common.vo.BuddhaLoadResoult;
import com.tanhua.sso.enums.SexEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import javax.swing.*;
import java.io.IOException;
import java.util.Map;

/**
 * @创建人 1anma0
 * @创建时间 2021/7/11
 * @描述
 */
@Service
public class UserInfoService {

    @Autowired
    private UserService userService;

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private BuddhaLoadService buddhaLoadService;

    @Autowired
    private FaceCheckService faceCheckService;

    /**
     * @描述 savaUserInfo：保存用户的信息
     * @参数 [前端传过来的参数Map, token]
     * 名称	类型	是否必须	默认值	备注	其他信息
     * gender	string
     * 性别 man woman
     * nickname	string
     * birthday	string
     * city	string
     * header	string
     * @返回值 是否添加成功Boolean
     * @创建人 1anma0
     * @创建时间 2021/7/11
     * @修改人和其它信息
     */
    public Boolean savaUserInfo(@RequestBody Map<String, String> map, String token) {
        //1.校验token
        User user = userService.chekToken(token);
        if (null == user) {
            return null;
        }
        //2.插入数据库
        UserInfo userInfo = new UserInfo();
        userInfo.setSex(StringUtils.equalsIgnoreCase(map.get("gender"), "man") ? SexEnum.MAN.getValue() : SexEnum.WOMAN.getValue());
        userInfo.setUserId(user.getId());
        userInfo.setNickName(map.get("nickname"));
        userInfo.setCity(map.get("city"));
        userInfo.setBirthday(map.get("birthday"));
        int insert = userInfoMapper.insert(userInfo);
        return insert == 1;
    }

    public Boolean saveBuddha(MultipartFile file, String token) {
        //1.校验token
        User user = userService.chekToken(token);
        if (null == user) {
            return false;
        }

        //2.检测是否为人脸

        boolean person = false;
        try {
            person = faceCheckService.isPerson(file.getBytes());
        } catch (IOException e) {
            return false;
        }
        if (!person) {
            return false;
        }
        //3.保存到阿里云
        //上传到aliyun.如果成功就返回图片地址;
        BuddhaLoadResoult buddhaLoadResoult = buddhaLoadService.buddhaLoad(file);
        if (!StringUtils.equals("done", buddhaLoadResoult.getBStatus())) {
            return false;
        }
        //4.将地址保存到UserInfo中
        //4.1创建Info对象,存入图片信息
        UserInfo userInfo = new UserInfo();
        userInfo.setLogo(buddhaLoadResoult.getBName());
        QueryWrapper queryWrapper = new QueryWrapper();

        queryWrapper.eq("user_id", user.getId());
        return userInfoMapper.update(userInfo, queryWrapper) == 1;

        //
    }


}
