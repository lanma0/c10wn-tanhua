package com.tanhua.sso.service;


import com.tanhua.sso.util.RedisUtil;
import com.tanhua.sso.vo.ErrorResult;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;


/**
 *
 * */

@Service
public class SmsService{
    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    JavaMailSenderImpl mailSender;

//    @Autowired
//    private RedisTemplate redisTemplate;

    public ErrorResult sendChekCode(String v1){
        String redisKey = "CHECK_CODE_" + v1;
        if (redisUtil.hasKey(redisKey)){
            String msg = "上一次发送的验证码还未失效！";
            return ErrorResult.builder().errCode("0000001").errMessage(msg).build();
        }
        String s = sendSms(v1);
        if (StringUtils.isEmpty(s)){
            String msg = "发送失败！";
            return ErrorResult.builder().errCode("0000000").errMessage(msg).build();
        }
        redisUtil.set(redisKey,s, 3*60);
        return null;
    }

    public String sendSms(String mobile){
//        String url = "https://sms-api.upyun.com/api/messages";
//        String mobile_code = ((Math.random()*9+1)*100000)+"" ;
        String mobile_code = 123456+"" ;

//        String content = "您的验证码是：" + mobile_code + "。请不要把验证码泄露给其他人。";
//        MultiValueMap<String, String> hm= new LinkedMultiValueMap<String, String>();
//        hm.set("template_id","1");
//        hm.set("Authorization","mMglH2MkMDVB2i8yP1Uscf6xlSoNs6");
//        hm.set("mobile",mobile);
//        hm.set("variables",mobile_code);


//        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url,hm , String.class);
//        System.out.println("responseEntity = " + responseEntity);
//        String body = responseEntity.getBody();
//        System.out.println("body = " + body);

//        try {
//            Document doc = DocumentHelper.parseText(body);
//            Element root = doc.getRootElement();
//
//            String code = root.elementText("code");
//            String msg = root.elementText("msg");
//            String smsid = root.elementText("smsid");
//            System.out.println("code = " + code);
//            System.out.println("msg = " + msg);
//            System.out.println("smsid = " + smsid);
//            if (StringUtils.equals(code,"2")){
//                return String.valueOf(mobile_code);
//            }
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        }
        return mobile_code;
    }


    public String sendSms1(String mobile){
        String url = "https://106.ihuyi.com/webservice/sms.php?method=Submit";
        int mobile_code = (int)((Math.random()*9+1)*100000) ;
        String content = "您的验证码是：" + mobile_code + "。请不要把验证码泄露给其他人。";
        MultiValueMap<String, String> hm= new LinkedMultiValueMap<String, String>();
        hm.set("account","C75621715");
        hm.set("password","820a69109a50763cdbc1bb8c629b82e2");
        hm.set("mobile",mobile);
        hm.set("content",content);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url,hm , String.class);
        System.out.println("responseEntity = " + responseEntity);
        String body = responseEntity.getBody();
        System.out.println("body = " + body);

        try {
            Document doc = DocumentHelper.parseText(body);
            Element root = doc.getRootElement();

            String code = root.elementText("code");
            String msg = root.elementText("msg");
            String smsid = root.elementText("smsid");
            System.out.println("code = " + code);
            System.out.println("msg = " + msg);
            System.out.println("smsid = " + smsid);
            if (StringUtils.equals(code,"2")){
                return String.valueOf(mobile_code);
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return null;
    }
    public String sendMail(String userMail){
        //复杂邮件
        int mobile_code = (int)((Math.random()*9+1)*100000) ;
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        try {
            //邮件发送助手
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            //邮件设置
            helper.setSubject("探花交友验证码");
            //使用 HTML 格式，true
            helper.setText("您的验证码是：【<b style='color:red'>"+mobile_code+"</b>】<br/>请不要告诉别人，有效期为3分钟",true);
            //接收者
            helper.setTo(userMail);
            //发送者
            helper.setFrom("878403789@qq.com");
            mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            e.printStackTrace();
            return null;
        }
        return String.valueOf(mobile_code);
    }
}
