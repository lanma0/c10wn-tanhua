package com.tanhua.sso.service;

import com.arcsoft.face.*;
import com.arcsoft.face.enums.DetectMode;
import com.arcsoft.face.enums.DetectOrient;
import com.arcsoft.face.enums.ErrorInfo;
import com.arcsoft.face.enums.ImageFormat;
import com.arcsoft.face.toolkit.ImageFactory;
import com.arcsoft.face.toolkit.ImageInfo;
import com.google.common.primitives.Bytes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * @创建人 1anma0
 * @创建时间 2021/7/10
 * @描述
 */
@Slf4j
@Service
public class FaceCheckService {

    @Value("${arcsoft.appid}")
    private String appid;

    @Value("${arcsoft.sdkKey}")
    private String sdkKey;

    @Value("${arcsoft.libPath}")
    private String libPath;

    private FaceEngine faceEngine;

    @PostConstruct
    public void init(){
        FaceEngine faceEngine = new FaceEngine(libPath);
        int errorCode = faceEngine.activeOnline(appid, sdkKey);
        if (errorCode != ErrorInfo.MOK.getValue() && errorCode != ErrorInfo.MERR_ASF_ALREADY_ACTIVATED.getValue()) {
            log.error("引擎激活失败");
            throw new RuntimeException("引擎激活失败");
        }
        //引擎配置
        EngineConfiguration engineConfiguration = new EngineConfiguration();
        engineConfiguration.setDetectMode(DetectMode.ASF_DETECT_MODE_IMAGE);
        engineConfiguration.setDetectFaceOrientPriority(DetectOrient.ASF_OP_ALL_OUT);
//        engineConfiguration.setDetectFaceMaxNum(10);
//        engineConfiguration.setDetectFaceScaleVal(16);

        //功能配置
        FunctionConfiguration functionConfiguration = new FunctionConfiguration();
        functionConfiguration.setSupportAge(true);
        functionConfiguration.setSupportFace3dAngle(true);
        functionConfiguration.setSupportFaceDetect(true);
        functionConfiguration.setSupportFaceRecognition(true);
        functionConfiguration.setSupportGender(true);
        functionConfiguration.setSupportLiveness(true);
        functionConfiguration.setSupportIRLiveness(true);
        engineConfiguration.setFunctionConfiguration(functionConfiguration);


        //初始化引擎
        errorCode = faceEngine.init(engineConfiguration);

        if (errorCode != ErrorInfo.MOK.getValue()) {
            log.error("初始化引擎出错!");
            throw new RuntimeException("初始化引擎出错!");
        }
        this.faceEngine =faceEngine;
    }

    //通过文件判断是否为人像
    public boolean isPerson(File file) {
        ImageInfo imageInfo = ImageFactory.getRGBData(file);
        //用于几首返回值信息的集合.
        List<FaceInfo> faceInfoList = new ArrayList<FaceInfo>();
        //图片的字节信息
        byte[] imageData = imageInfo.getImageData();
        //图片的宽度
        Integer width = imageInfo.getWidth();
        //图片的高度
        Integer height = imageInfo.getHeight();
        ImageFormat imageFormat = imageInfo.getImageFormat();

        faceEngine.detectFaces(imageData, width, height, imageFormat, faceInfoList);
        //如果是人像：[com.arcsoft.face.Rect(731, 188 - 1197, 654),1]
        //如果不是人像[];
        return !faceInfoList.isEmpty();

    }
        public boolean isPerson(byte[] fileByte){
            ImageInfo imageInfo = ImageFactory.getRGBData(fileByte);
            //用于几首返回值信息的集合.
            List<FaceInfo> faceInfoList = new ArrayList<FaceInfo>();
            //图片的字节信息
            byte[] imageData = imageInfo.getImageData();
            //图片的宽度
            Integer width = imageInfo.getWidth();
            //图片的高度
            Integer height = imageInfo.getHeight();
            ImageFormat imageFormat = imageInfo.getImageFormat();

            faceEngine.detectFaces(imageData, width, height, imageFormat, faceInfoList);
            //如果是人像：[com.arcsoft.face.Rect(731, 188 - 1197, 654),1]
            //如果不是人像[];
            return !faceInfoList.isEmpty();
        }
}
