package com.tanhua.sso;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;


@MapperScan("com.tanhua.common.mapper")
@SpringBootApplication(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class}) //排除mongo的自动配置
@ComponentScan(basePackages = {"com.tanhua.common","com.tanhua.sso"})

public class MyUserApplicaiton {
    public static void main(String[] args) {
        SpringApplication.run(MyUserApplicaiton.class,args);
    }

}
